# Global Customer Segmentation & Retention + Frequency Models



## 1. Background & Objectives
- GCS Framework that leverages different ML models as dimensions to create segments for all regions.
- Core dimensions are retention and frequency models.

## 2. Running Environments
- Legacy Engine: docker.repository.cloudera.com/cloudera/cdsw/engine:13-cml-2021.02-1

### 2.1 Required Packages
```txt
pyyaml==5.3.1
pandas==1.0.5
matplotlib==3.2.2
ipython==7.16.1
joblib==0.16.0
scikit-learn==0.23.1
numpy==1.18.5
seaborn==0.11.0
openpyxl==3.0.6
```

### 2.2 Spark Configurations
```conf
# compute instances configs
spark.driver.memory=4g
spark.driver.memoryOverhead=2000
spark.executor.cores=1
spark.executor.memory=4g
spark.executor.memoryOverhead=3000
spark.executor.instances=25
spark.kubernetes.allocation.batch.size=5

# adls location configs
spark.yarn.access.hadoopFileSystems=abfs://data@clouderastorageaccountpd.dfs.core.windows.net

# writing table configs
spark.sql.legacy.allowCreatingManagedTableUsingNonemptyLocation=true
spark.sql.hive.caseSensitiveInferenceMode=NEVER_INFER
spark.sql.autoBroadcastJoinThreshold=-1
```

### 2.3 Initialization Script
When first download the code repository to a blank project, do following to setup the folder.

```bash
pip3 install -r requirements.txt

mkdir log
mkdir -p output/scored/fpa
```

## 3. Module Notes
### 3.1 `sklearn_framework`
- `evaluations`
- `modeling`
- `preprocessing`
- `run`
- `utils`

### 3.2 `gcs`
- `excel_report_template` folder
- `gcs_profile`
- `gcs_segments`
- `run`
- `threshold.py`

## 4. Folders and Files
### 4.1 Folders
1. `churn_mb_dataprep`: spark script to prepare datasets
2. `configs`: configuration files for retention/frequency/segmentation
3. `models`: pre-trained models

### 4.2 Files
1. `run_model_main.py`: train/score retention/frequency model
2. `gcs_main.py`: create segmentation and generate profiles
3. `update_excel_report.py`: use profile reports to update excel report
4. `refresh_cml.sh` & `job.py`: run the job to prep data, score model, update profile, update end tables

## 5. Using the Code
### 5.1 Scoring
- Open a workbench session, in the command prompt in iPython shell (where `$dt` is a date string like '2021-05-01'):
```bash
!sh ./refresh_cml.sh $dt
```

- Automatically: `gcs_new_monthly` job has been setup in the CML `fpa_new` project. Runs on the 3th every month.