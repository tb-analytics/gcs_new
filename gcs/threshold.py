import pandas as pd
import numpy as np
from io import StringIO
import os
import itertools

"""
NOTE: Use the predefined percentile per model to find the correct threshold cut
"""

threshold_file = StringIO("""
region,model,type,percentile
NA,churn,existing,30
NA,churn,new,20
NA,mb,existing,35
NA,mb,new,30
EU,churn,existing,25
EU,churn,new,25
EU,mb,existing,35
EU,mb,new,25
AS,churn,existing,25
AS,churn,new,25
AS,mb,existing,30
AS,mb,new,35
JP,churn,existing,25
JP,churn,new,15
JP,mb,existing,25
JP,mb,new,15
""")

main_dir = "../models"
regions = ["NA", "EU", "AS", "JP"]
modelnames = ["churn", "mb"]
modeltypes = ["existing", "new"]

result_holder = list()
for rg, mn, mt in itertools.product(regions, modelnames, modeltypes):
    filedir = os.path.join(main_dir, mn, f"{rg}_{mt}_metadata")
    lgtable = pd.read_csv(os.path.join(filedir, "lift_gain_table.csv"))
    cmtable = pd.read_csv(os.path.join(filedir, "cm_all_threshold.csv"))
    mdf = pd.merge(lgtable, cmtable, left_on=np.round(lgtable["lowerbound_prob"], 2),
                   right_on=np.round(cmtable["index"], 2)).loc[:, ["percentile", "key_0", "cumlift", "err_rate"]] \
                   .rename(columns={"key_0": "threshold"})
    mdf["region"] = rg
    mdf["model"] = mn
    mdf["type"] = mt

    result_holder.append(mdf)


breakdown = pd.concat(result_holder, axis=0)
thresh_df = pd.read_csv(threshold_file, keep_default_na=False)
sum_df = pd.merge(thresh_df, breakdown, on=["region", "model", "type", "percentile"])

sum_df.to_csv(os.path.join(main_dir, "threshold_raw.csv"), index=False)
