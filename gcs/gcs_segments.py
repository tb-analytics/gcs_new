import os
import glob
import pandas as pd
from sklearn_framework.utils import make_dir_if_not_exist
# import matplotlib.pyplot as plt
# import numpy as np
# import seaborn as sns
# import sklearn
# from IPython.display import display
import logging

logger = logging.getLogger(__name__)


class GCSegmentation:
    def __init__(self, configs, region):
        self.region = region
        self.dt = configs['dt']
        self.threshold = configs['threshold']
        self.dataframes_holder = self._read_dataframes(configs, region)
        self.fulldf = self._merge_dataframes()
        self.seg_df = self.create_minisegments()
        self.segmentation_saving_path = make_dir_if_not_exist(configs['segmentation_files_output_dir'])

    @staticmethod
    def _read_dataframes(configs, region):
        import string
        from collections import defaultdict

        dataframes_holder = defaultdict(defaultdict)  # nested dictionary
        for file_types in ["profile_files", "dimensions_files"]:
            for element_key in configs[file_types]:
                # getting dynamic file names
                files_template = string.Template(os.path.join(*configs[file_types][element_key]["filespath"]))
                filesname = files_template.safe_substitute({"region": region, "dt": configs["dt"]})
                files_list = glob.glob(os.path.join(os.path.realpath(configs["home_dir"]), filesname))

                # read data files
                logger.info(f"[class GCSegmentation._read_dataframes] Reading {filesname}...")
                df = pd.concat((pd.read_csv(f, **configs["filereadingparams"]) for f in files_list),
                                ignore_index=True).fillna(configs["na_replacement_value"])

                cols_needed = configs[file_types][element_key]["cols_needed"]
                if cols_needed:
                    logger.info(f"[class GCSegmentation._read_dataframes] Keeping {cols_needed}")
                    df = df.loc[:, cols_needed]

                dataframes_holder[file_types][element_key] = df

        return dataframes_holder

    def _merge_dataframes(self) -> pd.DataFrame:
        # read data files
        from functools import reduce
        profiles = reduce(lambda x, y:
                          ("merged", pd.merge(x[1], y[1], on=["sk_goldencustomerid", "regions"],
                                     how="inner", suffixes=[f'_{x[0]}', f'_{y[0]}'])),
                          self.dataframes_holder["profile_files"].items())[1]
        profiles = profiles.loc[profiles['regions'] == self.region, :]

        # model dimensions, all based on sk_goldencustomerid for a specific region
        dims     = reduce(lambda x, y:
                          ("merged", pd.merge(x[1], y[1], on=["sk_goldencustomerid"],
                                     how="left", suffixes=[f'_{x[0]}', f'_{y[0]}'])),
                          self.dataframes_holder["dimensions_files"].items())[1]

        df = pd.merge(profiles, dims, on=["sk_goldencustomerid"], how="inner") \
             .rename(columns={'1_retention': 'churn_pos_prob', '1_frequency': 'mb_pos_prob'})
        return df

    def create_segment_quadrant(self, *args, **kwargs) -> pd.DataFrame:
        df = self.fulldf

        # reading threshold
        new_churn_threshold = self.threshold[self.region]['churn']['new']
        exs_churn_threshold = self.threshold[self.region]['churn']['existing']
        new_mulby_threshold = self.threshold[self.region]['mb']['new']
        exs_mulby_threshold = self.threshold[self.region]['mb']['existing']

        # adding segment
        df['segment'] = None
        df.loc[(df.definedgroup == 'new') &
               (df.churn_pos_prob >= new_churn_threshold) &
               (df.mb_pos_prob >= new_mulby_threshold), 'segment'] = 'I'
        df.loc[(df.definedgroup == 'new') &
               (df.churn_pos_prob >= new_churn_threshold) &
               (df.mb_pos_prob < new_mulby_threshold), 'segment'] = 'IV'
        df.loc[(df.definedgroup == 'new') &
               (df.churn_pos_prob < new_churn_threshold) &
               (df.mb_pos_prob >= new_mulby_threshold), 'segment'] = 'II'
        df.loc[(df.definedgroup == 'new') &
               (df.churn_pos_prob < new_churn_threshold) &
               (df.mb_pos_prob < new_mulby_threshold), 'segment'] = 'III'
        df.loc[(df.definedgroup == 'existing') &
               (df.churn_pos_prob >= exs_churn_threshold) &
               (df.mb_pos_prob >= exs_mulby_threshold), 'segment'] = 'I'
        df.loc[(df.definedgroup == 'existing') &
               (df.churn_pos_prob >= exs_churn_threshold) &
               (df.mb_pos_prob < exs_mulby_threshold), 'segment'] = 'IV'
        df.loc[(df.definedgroup == 'existing') &
               (df.churn_pos_prob < exs_churn_threshold) &
               (df.mb_pos_prob >= exs_mulby_threshold), 'segment'] = 'II'
        df.loc[(df.definedgroup == 'existing') &
               (df.churn_pos_prob < exs_churn_threshold) &
               (df.mb_pos_prob < exs_mulby_threshold), 'segment'] = 'III'

        return df

    def _north_america_minisegments(self) -> pd.DataFrame:
        df = self.create_segment_quadrant()
        assert all(df['regions'] == 'NA'), "[class GCSegmentation.north_america_minisegment_rules] Only North America " \
                                           "customers can use these rules."
        df['mini_segment'] = None
        for segment_nm in ['I', 'II', 'III', 'IV']:
            if segment_nm == 'I':
                # SEGMENT I
                df.loc[(df['segment'] == segment_nm) & (df['custtype'] == '3-Retained'), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['custtype'] != '3-Retained') &
                       (df['netsalesamt'] >  300), 'mini_segment'] = '2'
                df.loc[(df['segment'] == segment_nm) & (df['custtype'] != '3-Retained') &
                       (df['netsalesamt'] <= 300), 'mini_segment'] = '3'
            elif segment_nm == 'II':
                # SEGMENT II
                df.loc[(df['segment'] == segment_nm) & (df['visits'] >  1), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['visits'] == 1) &
                       (df['netsalesamt'] >  200), 'mini_segment'] = '2'
                df.loc[(df['segment'] == segment_nm) & (df['visits'] == 1) &
                       (df['netsalesamt'] <= 200), 'mini_segment'] = '3'
            elif segment_nm == 'III':
                # SEGMENT III
                df.loc[(df['segment'] == segment_nm) & (df['isomnichannel'] == 1), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['isomnichannel'] == 0) &
                       (df['netsalesamt'] >  300), 'mini_segment'] = '2'
                df.loc[(df['segment'] == segment_nm) & (df['isomnichannel'] == 0) &
                       (df['netsalesamt'] <= 300), 'mini_segment'] = '3'
            elif segment_nm == 'IV':
                df.loc[(df['segment'] == segment_nm), 'mini_segment'] = '1'
            else:
                raise ValueError("[class GCSegmentation.north_america_minisegment_rules] segment_nm should be within "
                                 "'I', 'II', 'III', 'IV'")

        if "fpa_score" in df.columns:
            df["fpa_segment"] = None
            df.loc[df["fpa_score"].isna(), 'fpa_segment'] = 'RO_ONLY'
            df.loc[df["fpa_score"] > 0.5, 'fpa_segment'] = "A"
            df.loc[df["fpa_score"] <= 0.5, 'fpa_segment'] = 'B'
            df.loc[df["fpa_segment"] == "RO_ONLY", "final_segment"] = df['fpa_segment'] + '_' + df['segment']
            df.loc[df["fpa_segment"] != "RO_ONLY", "final_segment"] = df['segment'] + df['mini_segment'] + df['fpa_segment']
        else:
            df['final_segment'] = df['segment'] + df['mini_segment']

        return df

    def _europe_minisegments(self) -> pd.DataFrame:
        df = self.create_segment_quadrant()
        assert all(df['regions'] == 'EU'), "[class GCSegmentation._europe_minisegments] Only Europe " \
                                           "customers can use these rules."
        df['mini_segment'] = None
        for segment_nm in ['I', 'II', 'III', 'IV']:
            if segment_nm == 'I':
                # SEGMENT I
                df.loc[(df['segment'] == segment_nm) & ((df['favchan'] != 'EC') | (df['isomnichannel'] != 0)),
                       'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & ((df['favchan'] == 'EC') & (df['isomnichannel'] == 0))
                       & (df['netsalesamt'] >= 200), 'mini_segment'] = '2'
                df.loc[(df['segment'] == segment_nm) & ((df['favchan'] == 'EC') & (df['isomnichannel'] == 0))
                       & (df['netsalesamt'] <  200), 'mini_segment'] = '3'

            elif segment_nm == 'II':
                # SEGMENT II
                df.loc[(df['segment'] == segment_nm), 'mini_segment'] = '1'
            elif segment_nm == 'III':
                # SEGMENT III
                df.loc[(df['regionleveltourism'] == 'Foreign') & (df['segment'] == segment_nm), 'mini_segment'] = '3'
                df.loc[(df['regionleveltourism'] == 'Domestic') & (df['segment'] == segment_nm) &
                       (df['pctmsrp'] >= 0.6), 'mini_segment'] = '1'
                df.loc[(df['regionleveltourism'] == 'Domestic') & (df['segment'] == segment_nm) &
                       (df['pctmsrp'] <  0.6), 'mini_segment'] = '2'
            elif segment_nm == 'IV':
                df.loc[(df['segment'] == segment_nm), 'mini_segment'] = '1'
            else:
                raise ValueError("[class GCSegmentation._europe_minisegments] segment_nm should be within "
                                 "'I', 'II', 'III', 'IV'")

        if "fpa_score" in df.columns:
            df["fpa_segment"] = None
            df.loc[df["fpa_score"].isna(), 'fpa_segment'] = 'RO_ONLY'
            df.loc[df["fpa_score"] > 0.5, 'fpa_segment'] = "A"
            df.loc[df["fpa_score"] <= 0.5, 'fpa_segment'] = 'B'
            df.loc[df["fpa_segment"] == "RO_ONLY", "final_segment"] = df['fpa_segment'] + '_' + df['segment']
            df.loc[df["fpa_segment"] != "RO_ONLY", "final_segment"] = df['segment'] + df['mini_segment'] + df['fpa_segment']
        else:
            df['final_segment'] = df['segment'] + df['mini_segment']

        return df

    def _apac_minisegments(self) -> pd.DataFrame:
        df = self.create_segment_quadrant()
        assert all(df['regions'] == 'AS'), "[class GCSegmentation._apac_minisegments] Only APAC " \
                                           "customers can use these rules."
        df['mini_segment'] = None
        for segment_nm in ['I', 'II', 'III', 'IV']:
            if segment_nm == 'I':
                # SEGMENT I
                df.loc[(df['segment'] == segment_nm) & (df['pctmsrp'] >= 0.8), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['pctmsrp'] <  0.8) & (df['custtype'] == '1-New') &
                       (df['visits'] == 1), 'mini_segment'] = '2'
                df.loc[(df['segment'] == segment_nm) & (df['pctmsrp'] < 0.8) & ((df['custtype'] != '1-New') |
                       (df['visits'] != 1)), 'mini_segment'] = '3'
            elif segment_nm == 'II':
                # SEGMENT II
                df.loc[(df['segment'] == segment_nm) & (df['pctmsrp'] >= 0.8), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['pctmsrp'] <  0.8), 'mini_segment'] = '2'
            elif segment_nm == 'III':
                # SEGMENT III
                df.loc[(df['segment'] == segment_nm) & (df['ismulticategorybuyer'] == 1), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['ismulticategorybuyer'] == 0) &
                       (df['pctmsrp'] >= 0.8), 'mini_segment'] = '2'
                df.loc[(df['segment'] == segment_nm) & (df['ismulticategorybuyer'] == 0) &
                       (df['pctmsrp'] < 0.8), 'mini_segment'] = '3'
            elif segment_nm == 'IV':
                df.loc[(df['segment'] == segment_nm), 'mini_segment'] = '1'
            else:
                raise ValueError("[class GCSegmentation._apac_minisegments] segment_nm should be within "
                                 "'I', 'II', 'III', 'IV'")

        if "fpa_score" in df.columns:
            df["fpa_segment"] = None
            df.loc[df["fpa_score"].isna(), 'fpa_segment'] = 'RO_ONLY'
            df.loc[df["fpa_score"] > 0.5, 'fpa_segment'] = "A"
            df.loc[df["fpa_score"] <= 0.5, 'fpa_segment'] = 'B'
            df.loc[df["fpa_segment"] == "RO_ONLY", "final_segment"] = df['fpa_segment'] + '_' + df['segment']
            df.loc[df["fpa_segment"] != "RO_ONLY", "final_segment"] = df['segment'] + df['mini_segment'] + df['fpa_segment']
        else:
            df['final_segment'] = df['segment'] + df['mini_segment']

        return df

    def _japan_minisegments(self) -> pd.DataFrame:
        df = self.create_segment_quadrant()
        assert all(df['regions'] == 'JP'), "[class GCSegmentation._japan_minisegments] Only Japan " \
                                           "customers can use these rules."
        df['mini_segment'] = None
        for segment_nm in ['I', 'II', 'III', 'IV']:
            if segment_nm == 'I':
                # SEGMENT I
                df.loc[(df['segment'] == segment_nm) & (df['pctmsrp'] >= 0.6), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['pctmsrp'] <  0.6), 'mini_segment'] = '2'
            elif segment_nm == 'II':
                # SEGMENT II
                df.loc[(df['segment'] == segment_nm) & (df['netsalesamt'] >= 300), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['netsalesamt'] <  300), 'mini_segment'] = '2'
            elif segment_nm == 'III':
                # SEGMENT III
                df.loc[(df['segment'] == segment_nm) & (df['netsalesamt'] >= 325), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['netsalesamt'] <  325) & (df['pctmsrp'] >= 0.6),
                       'mini_segment'] = '2'
                df.loc[(df['segment'] == segment_nm) & (df['netsalesamt'] <  325) & (df['pctmsrp'] <  0.6),
                       'mini_segment'] = '3'
            elif segment_nm == 'IV':
                df.loc[(df['segment'] == segment_nm), 'mini_segment'] = '1'
            else:
                raise ValueError("[class GCSegmentation._japan_minisegments] segment_nm should be within "
                                 "'I', 'II', 'III', 'IV'")

        if "fpa_score" in df.columns:
            df["fpa_segment"] = None
            df.loc[df["fpa_score"].isna(), 'fpa_segment'] = 'RO_ONLY'
            df.loc[df["fpa_score"] > 0.5, 'fpa_segment'] = "A"
            df.loc[df["fpa_score"] <= 0.5, 'fpa_segment'] = 'B'
            df.loc[df["fpa_segment"] == "RO_ONLY", "final_segment"] = df['fpa_segment'] + '_' + df['segment']
            df.loc[df["fpa_segment"] != "RO_ONLY", "final_segment"] = df['segment'] + df['mini_segment'] + df['fpa_segment']
        else:
            df['final_segment'] = df['segment'] + df['mini_segment']

        return df

    def create_minisegments(self) -> pd.DataFrame:
        logger.info(f"[class GCSegmentation.create_minisegments] Creating {self.region} mini-segments...")
        if self.region == 'NA':
            return self._north_america_minisegments()
        elif self.region == 'EU':
            return self._europe_minisegments()
        elif self.region == 'AS':
            return self._apac_minisegments()
        elif self.region == 'JP':
            return self._japan_minisegments()
        else:
            raise ValueError("[class GCSegmentation.create_minisegments] Region value not valid.")

    def save_minisegments(self):
        df = self.seg_df.copy().round(4)
        if 'fpa_score' in df.columns:
            columns_needed = ['sk_goldencustomerid', 'regions', 'final_segment', 'churn_pos_prob', 'mb_pos_prob', 'fpa_score']
        else:
            columns_needed = ['sk_goldencustomerid', 'regions', 'final_segment', 'churn_pos_prob', 'mb_pos_prob']
        saving_path = os.path.join(self.segmentation_saving_path, f'{self.region}_segmentation_{self.dt}.csv')
        logger.info(f"[class GCSegmentation.save_minisegments] Saving {saving_path}...")
        df[columns_needed].to_csv(saving_path, index=False, header=True)
        return
