import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import logging
from sklearn_framework.utils import save_plot

logger = logging.getLogger(__name__)


class SegmentProfile:
    def __init__(self, population_df, region, dt):
        # population here is R12M regional customers
        self.population_df = population_df
        self.region = region
        self.dt = dt
        self.outlet_segments = ["RO_ONLY_I", "RO_ONLY_II", "RO_ONLY_III", "RO_ONLY_IV"]

    @property
    def actual_retention_rate(self):
        actual_retention_rate = self.population_df['isretained'].sum() / \
                                self.population_df['sk_goldencustomerid'].count()
        return actual_retention_rate

    @property
    def actual_mb_rate(self):
        actual_mb_rate = self.population_df['ismultibuyernextyear'].sum() / \
                         self.population_df['sk_goldencustomerid'].count()
        return actual_mb_rate

    @property
    def actual_retention_mb_rates_by_cust_type(self):
        actual_retention_mb_rates_by_cust_type = self.population_df.groupby('definedgroup').agg({
            'isretained': 'sum',
            'ismultibuyernextyear': 'sum'
        }).T / self.population_df.groupby('definedgroup')['sk_goldencustomerid'].count()
        return actual_retention_mb_rates_by_cust_type


class AllSegmentsStatsProfile(SegmentProfile):
    def __init__(self, population_df, region, dt, segmentation_id_cols=None):
        super().__init__(population_df, region, dt)
        self.segmentation_id_cols = segmentation_id_cols if segmentation_id_cols else ['regions']

    def profile_stats(self):
        import statistics

        def _safe_median(gen):
            """
            If a median needs to be calculated on an empty list, return NaN
            :param gen: a generator from pandas groupby
            :return: median or NaN
            """
            try:
                return statistics.median(gen)
            except Exception:
                return np.nan

        def _safe_mean(gen):
            """
            If a mean needs to be calculated on an empty list, return NaN
            :param gen: a generator from pandas groupby
            :return: median or NaN
            """
            try:
                return statistics.mean(gen)
            except Exception:
                return np.nan

        df = self.population_df.copy()
        df['r12m_total_items'] = df['fp_only_itm_cnts'] + df['md_only_itm_cnts'] + \
                                 df['promo_only_itm_cnts'] + df['md_n_promo_itm_cnts']
        df['r12m_fp_item_rate'] = df['fp_only_itm_cnts'] / df['r12m_total_items']
        df['r12m_md_item_rate'] = df['md_only_itm_cnts'] / df['r12m_total_items']
        df['r12m_dsct_item_rate'] = df['promo_only_itm_cnts'] / df['r12m_total_items']
        df['r12m_md_x_dsct_item_rate'] = df['md_n_promo_itm_cnts'] / df['r12m_total_items']

        all_cats = ['sport_newness', 'jewelry_newness', 'handbags_newness', 'shoes_newness', 'slg_newness',
                    'rtw_newness']
        df['majorcatshopped'] = np.sum(df.loc[:, all_cats] >= 0, axis=1)
        df.loc[df['majorcatshopped'] == 0, 'majorcatshopped'] = 1  # group others as 1 cat

        agg_mappings = {
            'custcnt': ('sk_goldencustomerid', 'count'),
            'isretained': ('isretained', 'sum'),
            'ismultibuyernextyear': ('ismultibuyernextyear', 'sum'),
            'definedgroup': ('definedgroup', lambda x: sum(1 if i == 'new' else 0 for i in x)),
            'r12m_new_cust': ('custtype', lambda x: sum(1 if i == '1-New' else 0 for i in x)),
            'r12m_react_cust': ('custtype', lambda x: sum(1 if i == '2-Reactivated' else 0 for i in x)),
            'r12m_retained_cust': ('custtype', lambda x: sum(1 if i == '3-Retained' else 0 for i in x)),
            'churn_pos_prob': ('churn_pos_prob', 'mean'),
            'mb_pos_prob': ('mb_pos_prob', 'mean'),
            'countryleveltourism': ('countryleveltourism', lambda x: sum(1 if i == 'Foreign' else 0 for i in x)),
            'regionleveltourism': ('regionleveltourism', lambda x: sum(1 if i == 'Foreign' else 0 for i in x)),
            'sport_newness_med': ('sport_newness', lambda x: _safe_median(i for i in x if i > 0)),
            'jewelry_newness_med': ('jewelry_newness', lambda x: _safe_median(i for i in x if i > 0)),
            'handbags_newness_med': ('handbags_newness', lambda x: _safe_median(i for i in x if i > 0)),
            'shoes_newness_med': ('shoes_newness', lambda x: _safe_median(i for i in x if i > 0)),
            'slg_newness_med': ('slg_newness', lambda x: _safe_median(i for i in x if i > 0)),
            'rtw_newness_med': ('rtw_newness', lambda x: _safe_median(i for i in x if i > 0)),
            'r12m_purchased_rf': ('purchased_rf', 'sum'),
            'r12m_purchased_ec': ('purchased_ec', 'sum'),
            'r12m_purchased_ro': ('purchased_ro', 'sum'),
            'r12m_isomnichannel': ('r12m_isomnichannel', 'sum'),
            'r12m_ismulticategorybuyer': ('r12m_ismulticategorybuyer', 'sum'),
            'r12m_majorcatshopped': ('majorcatshopped', 'mean'),
            'r12m_netsalesamt': ('netsalesamt', lambda x: statistics.mean(i for i in x if i > 0)),
            'r12m_netsalesamt_total': ('netsalesamt', 'sum'),
            'r12m_avgsalesmkdnrate': ('avgsalesmkdnrate', lambda x: statistics.mean(i for i in x if 1 >= i >= 0)),
            'r12m_avgsalesdsctrate': ('avgsalesdsctrate', lambda x: statistics.mean(i for i in x if 1 >= i >= 0)),
            'r12m_avgfullpricerate_dollars': ('avgfullpricerate_dollars',
                                              lambda x: statistics.mean(i for i in x if 1 >= i >= 0)),
            'r12m_avgfullpricerate_units': ('avgfullpricerate_units',
                                            lambda x: statistics.mean(i for i in x if 1 >= i >= 0)),
            'r12m_bought_fp_only_itm': ('fp_only_itm_cnts', lambda x: sum(1 for i in x if i > 0)),
            'r12m_bought_md_only_itm': ('md_only_itm_cnts', lambda x: sum(1 for i in x if i > 0)),
            'r12m_bought_dsct_only_itm': ('promo_only_itm_cnts', lambda x: sum(1 for i in x if i > 0)),
            'r12m_bought_md_x_dsct_itm': ('md_n_promo_itm_cnts', lambda x: sum(1 for i in x if i > 0)),
            'r12m_fp_only': ('r12m_fp_item_rate', lambda x: sum(1 for i in x if i == 1)),
            'r12m_offprice_only': ('r12m_fp_item_rate', lambda x: sum(1 for i in x if i == 0)),
            'r12m_mix_price': ('r12m_fp_item_rate', lambda x: sum(1 for i in x if 0 < i < 1)),
            'r12m_fp_item_rate': ('r12m_fp_item_rate', 'mean'),
            'r12m_md_item_rate': ('r12m_md_item_rate', 'mean'),
            'r12m_dsct_item_rate': ('r12m_dsct_item_rate', 'mean'),
            'r12m_md_x_dsct_item_rate': ('r12m_md_x_dsct_item_rate', 'mean'),
            'r12m_visits': ('visits', 'mean'),
            'r12m_ismultibuyers': ('visits', lambda x: sum(1 if i > 1 else 0 for i in x)),
            'r12m_aov': ('aov', lambda x: statistics.mean(i for i in x if i > 0)),
            'r12m_aur': ('aur', lambda x: statistics.mean(i for i in x if i > 0)),
            'r12m_upt': ('upt', lambda x: statistics.mean(i for i in x if i > 0)),
            # use newness to determine cat shopped, a prodcat shopped but without valid launchdt has newness
            # value of 0
            'shopped_rtw': ('rtw_newness', lambda x: sum(1 if i >= 0 else 0 for i in x)),
            'shopped_slg': ('slg_newness', lambda x: sum(1 if i >= 0 else 0 for i in x)),
            'shopped_shoes': ('shoes_newness', lambda x: sum(1 if i >= 0 else 0 for i in x)),
            'shopped_handbags': ('handbags_newness', lambda x: sum(1 if i >= 0 else 0 for i in x)),
            'shopped_jewelry': ('jewelry_newness', lambda x: sum(1 if i >= 0 else 0 for i in x)),
            'shopped_sport': ('sport_newness', lambda x: sum(1 if i >= 0 else 0 for i in x)),
            'r12m_rtw_fp_pct': ('rtw_fp_pct', lambda x: _safe_mean(i for i in x if i > 0)),
            'r12m_slg_fp_pct': ('slg_fp_pct', lambda x: _safe_mean(i for i in x if i > 0)),
            'r12m_shoes_fp_pct': ('shoes_fp_pct', lambda x: _safe_mean(i for i in x if i > 0)),
            'r12m_handbags_fp_pct': ('handbags_fp_pct', lambda x: _safe_mean(i for i in x if i > 0)),
            'r12m_jewelry_fp_pct': ('jewelry_fp_pct', lambda x: _safe_mean(i for i in x if i > 0)),
            'r12m_sport_fp_pct': ('sport_fp_pct', lambda x: _safe_mean(i for i in x if i > 0)),
            'marketable': ('marketable', lambda x: sum(1 if i == 'Y' else 0 for i in x)),
            'emailable': ('emailable', lambda x: sum(1 if i == 'Y' else 0 for i in x)),
            'phoneable': ('phoneable', lambda x: sum(1 if i == 'Y' else 0 for i in x)),
            'mailable': ('mailable', lambda x: sum(1 if i == 'Y' else 0 for i in x)),
            'ltv': ('ltv', 'mean'),
        }

        _t = df.groupby(self.segmentation_id_cols).agg(**agg_mappings)
        _t['regionalpct'] = _t['custcnt'] / df['sk_goldencustomerid'].count()
        _t['regionalsalespct'] = _t['r12m_netsalesamt_total'] / df['netsalesamt'].sum()

        # fp only KPIs
        _t['regionalpct_fpc'] = _t['custcnt'] / df.loc[df['fpa_segment'] != 'RO_ONLY', 'sk_goldencustomerid'].count()
        _t['regionalsalespct_fpc'] = _t['r12m_netsalesamt_total'] / df.loc[df['fpa_segment'] != 'RO_ONLY', 'netsalesamt'].sum()

        _t['actual_retention_rate'] = _t['isretained'] / _t['custcnt']
        _t['actual_mb_rate'] = _t['ismultibuyernextyear'] / _t['custcnt']
        _t['diff_country_tourists_rate'] = _t['countryleveltourism'] / _t['custcnt']
        _t['diff_region_tourists_rate'] = _t['regionleveltourism'] / _t['custcnt']
        _t['r12m_rf_rate'] = _t['r12m_purchased_rf'] / _t['custcnt']
        _t['r12m_ec_rate'] = _t['r12m_purchased_ec'] / _t['custcnt']
        _t['r12m_ro_rate'] = _t['r12m_purchased_ro'] / _t['custcnt']
        _t['r12m_new_rate'] = _t['r12m_new_cust'] / _t['custcnt']
        _t['r12m_react_rate'] = _t['r12m_react_cust'] / _t['custcnt']
        _t['r12m_retained_rate'] = _t['r12m_retained_cust'] / _t['custcnt']
        _t['r12m_omnichannel_rate'] = _t['r12m_isomnichannel'] / _t['custcnt']
        _t['r12m_multicategory_rate'] = _t['r12m_ismulticategorybuyer'] / _t['custcnt']
        _t['r12m_ismultibuyers_rate'] = _t['r12m_ismultibuyers'] / _t['custcnt']
        _t['shopped_rtw_rate'] = _t['shopped_rtw'] / _t['custcnt']
        _t['shopped_slg_rate'] = _t['shopped_slg'] / _t['custcnt']
        _t['shopped_shoes_rate'] = _t['shopped_shoes'] / _t['custcnt']
        _t['shopped_handbags_rate'] = _t['shopped_handbags'] / _t['custcnt']
        _t['shopped_jewelry_rate'] = _t['shopped_jewelry'] / _t['custcnt']
        _t['shopped_sport_rate'] = _t['shopped_sport'] / _t['custcnt']
        _t['r12m_bought_fp_only_itm_rate'] = _t['r12m_bought_fp_only_itm'] / _t['custcnt']
        _t['r12m_bought_md_only_itm_rate'] = _t['r12m_bought_md_only_itm'] / _t['custcnt']
        _t['r12m_bought_dsct_only_itm_rate'] = _t['r12m_bought_dsct_only_itm'] / _t['custcnt']
        _t['r12m_bought_md_x_dsct_itm_rate'] = _t['r12m_bought_md_x_dsct_itm'] / _t['custcnt']
        _t['r12m_fp_only_rate'] = _t['r12m_fp_only'] / _t['custcnt']
        _t['r12m_offprice_only_rate'] = _t['r12m_offprice_only'] / _t['custcnt']
        _t['r12m_mix_price_rate'] = _t['r12m_mix_price'] / _t['custcnt']
        _t['marketable_rate'] = _t['marketable'] / _t['custcnt']
        _t['emailable_rate'] = _t['emailable'] / _t['custcnt']
        _t['phoneable_rate'] = _t['phoneable'] / _t['custcnt']
        _t['mailable_rate'] = _t['mailable'] / _t['custcnt']
        _t['r12m_one_timer_rate'] = 1 - _t['r12m_ismultibuyers_rate']

        report_seq = [
            'custcnt',
            'regionalpct',
            'regionalpct_fpc',
            'r12m_netsalesamt_total',
            'regionalsalespct',
            'regionalsalespct_fpc',
            'churn_pos_prob',
            'mb_pos_prob',
            'r12m_new_rate',
            'r12m_react_rate',
            'r12m_retained_rate',
            'actual_retention_rate',
            'actual_mb_rate',
            'r12m_rf_rate',
            'r12m_ec_rate',
            'r12m_ro_rate',
            'r12m_omnichannel_rate',
            'r12m_multicategory_rate',
            'r12m_majorcatshopped',
            'diff_country_tourists_rate',
            'diff_region_tourists_rate',
            'shopped_rtw_rate',
            'shopped_slg_rate',
            'shopped_shoes_rate',
            'shopped_handbags_rate',
            'shopped_jewelry_rate',
            'shopped_sport_rate',
            'rtw_newness_med',
            'sport_newness_med',
            'handbags_newness_med',
            'shoes_newness_med',
            'jewelry_newness_med',
            'slg_newness_med',
            'r12m_rtw_fp_pct',
            'r12m_slg_fp_pct',
            'r12m_shoes_fp_pct',
            'r12m_handbags_fp_pct',
            'r12m_jewelry_fp_pct',
            'r12m_sport_fp_pct',
            'r12m_netsalesamt',
            'r12m_avgsalesmkdnrate',
            'r12m_avgsalesdsctrate',
            'r12m_avgfullpricerate_dollars',
            'r12m_avgfullpricerate_units',
            'r12m_bought_fp_only_itm_rate',
            'r12m_bought_md_only_itm_rate',
            'r12m_bought_dsct_only_itm_rate',
            'r12m_bought_md_x_dsct_itm_rate',
            'r12m_fp_item_rate',
            'r12m_md_item_rate',
            'r12m_dsct_item_rate',
            'r12m_md_x_dsct_item_rate',
            'r12m_fp_only_rate',
            'r12m_offprice_only_rate',
            'r12m_mix_price_rate',
            'r12m_visits',
            'r12m_one_timer_rate',
            'r12m_aov',
            'r12m_aur',
            'r12m_upt',
            'marketable_rate',
            'emailable_rate',
            'phoneable_rate',
            'mailable_rate',
            'ltv'
        ]

        report = _t.loc[:, report_seq].fillna(0).T
        # report.columns = report.columns.to_flat_index()  # flatten multi-index
        report.columns = [''.join(col).strip() for col in report.columns.values]
        return report

    def profile_stats_report(self, report_path=None, plot_position=False, *args, **kwargs):
        import inspect
        from sklearn_framework.utils import make_dir_if_not_exist
        report_template = '''\
            {region} {dt} Profile Report
            ===========
            Actual Retention Rate: {retention_rate}
            Actual Multi-Purchases Rate: {mb_rate}

            Break-down:
            ===========
            {breakdown}
            
            Actual Lift:
            ===========
            {lift}
            
            Full Stats:
            ===========
            {profile_stats_report}
            '''

        breakdown = self.actual_retention_mb_rates_by_cust_type

        ps = self.profile_stats()
        # All channel all status
        population_ps = AllSegmentsStatsProfile(self.population_df, "All", self.dt, ['regions']).profile_stats()
        # FPC all stats
        _mask = ~self.population_df["final_segment"].isin(self.outlet_segments)
        population_ps_fpc = AllSegmentsStatsProfile(self.population_df[_mask], "All", self.dt, ['regions']).profile_stats()

        ps.loc[["regionalpct_fpc", "regionalsalespct_fpc"], self.outlet_segments] = 0.
        population_ps.loc[["regionalpct_fpc", "regionalsalespct_fpc"]] = 1.

        lift_retention = (ps.loc['r12m_react_rate'] + ps.loc['r12m_new_rate']) * breakdown.loc['isretained', 'new'] + \
                          ps.loc['r12m_retained_rate'] * breakdown.loc['isretained', 'existing']
        lift_mb = (ps.loc['r12m_react_rate'] + ps.loc['r12m_new_rate']) * breakdown.loc['ismultibuyernextyear', 'new'] + \
                   ps.loc['r12m_retained_rate'] * breakdown.loc['ismultibuyernextyear', 'existing']

        lift = pd.concat([ps.loc['actual_retention_rate'] / lift_retention,
                          ps.loc['actual_mb_rate'] / lift_mb], axis=1) \
                 .rename(columns={0: 'isretained', 1: 'ismultibuyernextyear'}).T

        final_stats = pd.concat([population_ps_fpc, ps, population_ps], axis=1)

        report = inspect.cleandoc(report_template).format(
            region=self.region,
            dt=self.dt,
            retention_rate="{0:.2f}".format(self.actual_retention_rate),
            mb_rate="{0:.2f}".format(self.actual_mb_rate),
            breakdown=breakdown.to_string(float_format='{:.2f}'.format),
            lift=lift.to_string(float_format='{:.2f}'.format),
            profile_stats_report=final_stats.to_string(float_format='{:.2f}'.format)
        )

        if report_path:
            report_path = make_dir_if_not_exist(report_path)
            with open(os.path.join(report_path, f'segmentreport_{self.dt}.txt'), 'w+') as f:
                f.writelines(report)
            # summarize the profile
            sub_report_path = make_dir_if_not_exist(os.path.join(report_path, 'report_source_csv'))
            with open(os.path.join(sub_report_path, f'{self.region}_segmentprofile_{self.dt}.csv'), 'w+') as f:
                final_stats.fillna(0.).round(4).to_csv(f, header=True, index=True)
        else:
            print(report)

        # plot relative positions of segments/mini-segments
        if plot_position:
            segment_labels = final_stats.columns[1:]
            _x = final_stats.loc['actual_retention_rate', segment_labels]
            _y = final_stats.loc['actual_mb_rate', segment_labels]
            fig, ax = plt.subplots()
            ax.scatter(_x, _y)
            for i, txt in enumerate(segment_labels):
                ax.annotate("".join(txt), (_x[i], _y[i]))
            save_plot(fig, report_path, f"segment_positions_{self.dt}")

        return


class SingleSegmentIndexingProfile(SegmentProfile):
    def __init__(self, population_df, region, dt, segmentation_identifier):
        """
        :param population_df: dataframe that represent regional population
        :param segmentation_identifier: should be an ordered dict with hierarchy
        """
        super().__init__(population_df, region, dt)
        self.segment_df = self._filter_segment(segmentation_identifier)
        self.segment_id = "".join(segmentation_identifier.values())

    def _filter_segment(self, segmentation_identifier):
        mask = (self.population_df.loc[:, list(segmentation_identifier)] == pd.Series(segmentation_identifier)).all(
            axis=1)
        return self.population_df.loc[mask, :]

    @staticmethod
    def numericprofile(df, bucket, col):
        ttl_cust = df.count()['sk_goldencustomerid']
        final = df.groupby(pd.cut(df[col], bucket, right=False)).agg(
            _max=(col, 'max'),
            _min=(col, 'min'),
            custcnt=('sk_goldencustomerid', 'count'),
            # custpct=('sk_goldencustomerid', lambda x: x.count() / ttl_cust)
        ).fillna(0)
        final['custpct'] = final['custcnt'] / ttl_cust
        return final

    @staticmethod
    def categoricalprofile(df, col, orders=None):
        ttl_cust = df.count()['sk_goldencustomerid']
        final = df.groupby(col).agg(
            custcnt=('sk_goldencustomerid', 'count'),
            # custpct=('sk_goldencustomerid', lambda x: x.count() / ttl_cust)
        ).fillna(0)
        final['custpct'] = final['custcnt'] / ttl_cust
        if orders:
            final = final.reindex(orders)

        return final

    def zero2oneprofile(self, df, col):
        bucket = np.arange(0, 1.05, 0.1)
        bucket = np.insert(bucket, len(bucket), float('inf'))
        final = self.numericprofile(df, bucket, col)
        return final

    def newexprofile(self, df):
        return self.categoricalprofile(df, 'custtype')

    def netsalesprofile(self, df):
        # pricebucket = np.arange(0, 1100, 100.0)
        # pricebucket = np.insert(pricebucket, len(pricebucket), float('inf'))
        pricebucket = [0, 400, 800, float('inf')]  # breakdown per profile reporting needs
        final = self.numericprofile(df, pricebucket, 'netsalesamt')
        return final

    def visitsprofile(self, df):
        # bucket = np.arange(1, 11, 1.0)
        # bucket = np.insert(bucket, len(bucket), float('inf'))
        bucket = [1, 2, 3, float('inf')]  # breakdown per profile reporting needs
        final = self.numericprofile(df, bucket, 'visits')
        return final

    def aovprofile(self, df):
        bucket = np.arange(0, 1100, 200.0)
        bucket = np.insert(bucket, len(bucket), float('inf'))
        final = self.numericprofile(df, bucket, 'aov')
        return final

    def aurprofile(self, df):
        bucket = np.arange(0, 600, 100.0)
        bucket = np.insert(bucket, len(bucket), float('inf'))
        final = self.numericprofile(df, bucket, 'aur')
        return final

    def uptprofile(self, df):
        bucket = np.arange(1, 11, 1.0)
        bucket = np.insert(bucket, len(bucket), float('inf'))
        final = self.numericprofile(df, bucket, 'upt')
        return final

    def pctmsrpprofile(self, df):
        return self.zero2oneprofile(df, 'pctmsrp')

    def markdownprofile(self, df):
        return self.zero2oneprofile(df, 'avgsalesmkdnrate')

    def promodiscountprofile(self, df):
        return self.zero2oneprofile(df, 'avgsalesdsctrate')

    # def channelprofile(self, df):
    #     return self.categoricalprofile(df, ['r12m_isomnichannel', 'favchan'])

    def tourismprofile(self, df):
        return self.categoricalprofile(df, ['countryleveltourism', 'regionleveltourism'])

    # def preferredcategoryprofile(self, df):
    #     return self.categoricalprofile(df, 'mostcatspend')

    def generationprofile(self, df):
        return self.categoricalprofile(df, 'generation',
                                       orders=['silentgen', 'babyboomers', 'genx', 'millennials', 'genz', 0])

    def agegroupprofile(self, df):
        return self.categoricalprofile(df, 'agegroupdesc')

    def incomeprofile(self, df):
        return self.categoricalprofile(df, 'hhincomerangedesc', orders=[0,
                                                                        '$1,000-$14,999',
                                                                        '$15,000-$24,999',
                                                                        '$25,000-$34,999',
                                                                        '$35,000-$49,999',
                                                                        '$50,000-$74,999',
                                                                        '$75,000-$99,999',
                                                                        '$100,000-$124,999',
                                                                        '$125,000-$149,999',
                                                                        '$150,000-$174,999',
                                                                        '$175,000-$199,999',
                                                                        '$200,000-$249,999',
                                                                        '$250,000+',
                                                                        ])

    def mosaicprofile(self, df):
        # def adj_mosaic_grp(row):
        #     adj_grp = row
        #     if row in ('A', 'B', 'D', 'F'):
        #         pass
        #     elif row in ('C', 'E'):
        #         adj_grp = 'C/E'
        #     elif row in ('O', 'G', 'K'):
        #         adj_grp = 'O/G/K'
        #     elif row == "":
        #         adj_grp = 'Unknown'
        #     else:
        #         adj_grp = 'Others'
        #     return adj_grp
        #
        # df.loc[:, 'mosaic_grp_adj'] = df.loc[:, 'mosaic_grp'].apply(adj_mosaic_grp).values
        # return self.categoricalprofile(df, 'mosaic_grp_adj')
        return self.categoricalprofile(df, 'mosaic_grp')

    def channelbreakdownprofile(self, df):
        df.loc[:, 'rf_ec_ro'] = df['purchased_rf'].astype(str) + df['purchased_ec'].astype(str) \
                         + df['purchased_ro'].astype(str)
        return self.categoricalprofile(df, 'rf_ec_ro', orders=['100', '010', '001', '110', '101', '011', '111'])

    def ethnicprofile(self, df):
        return self.categoricalprofile(df, 'ethnic_grp')

    def newnessprofile(self, df):
        bucket = np.arange(0, 365, 30.0)
        bucket = np.insert(bucket, len(bucket), float('inf'))
        final = self.numericprofile(df, bucket, 'newness')
        return final

    def preferredmthprofile(self, df):
        return self.categoricalprofile(df, 'preferredmth')

    @staticmethod
    def plot_barh_ax(df, ax, xlim=2):
        y = df.values.ravel()
        df.plot(kind='barh', color='lightseagreen', ax=ax)

        # add text for outside values
        for i, v in enumerate(y):
            if abs(v) >= xlim:
                symbol = 1 if v > 0 else -1
                ax.text(symbol * xlim * 0.88, i, str("%.2f" % v), color='white',
                        fontweight='bold', ha='center', va='center')

        ax.axvline(0, linewidth=0.5, c='gray')
        ax.set_xlim([-xlim, xlim])

        titlename = '_'.join(name for name in df.index.names if name is not None)
        ax.set_title(titlename)
        return

    def _profile_dataframe(self, profilefunc_str):
        profilefunc = getattr(self, profilefunc_str)
        population = profilefunc(self.population_df)
        subseg = profilefunc(self.segment_df)
        grpdf = subseg.div(population)[['custpct']] - 1
        idx_df = pd.concat([
            population['custpct'].rename('population'),
            subseg['custpct'].rename('mini_seg'),
            grpdf['custpct'].rename('index')
        ], axis=1)
        return idx_df

    def profile_text_report(self, profilefunclst, report_path=None):
        from sklearn_framework.utils import make_dir_if_not_exist
        for profilefunc_str in profilefunclst:
            logger.info(f"[SingleSegmentIndexingProfile.profile_text_report] Running {profilefunc_str}")
            idx_df = self._profile_dataframe(profilefunc_str)

            if report_path:
                report_path = make_dir_if_not_exist(report_path)
                idx_df.fillna(0.).to_csv(os.path.join(report_path,
                                                      f'index_breakdown_{self.segment_id}_{profilefunc_str}_{self.dt}.csv'))
            else:
                print(idx_df.to_string())

    def profile_indexing_graph_report(self, profilefunclst, report_path=None):
        from matplotlib.gridspec import GridSpec

        def graphgrid(graphnum):
            import math
            _nrows = math.floor(graphnum ** 0.5)
            _ncols = math.ceil(graphnum / _nrows)
            return _nrows, _ncols

        nrows, ncols = graphgrid(len(profilefunclst))
        fig = plt.figure(figsize=(ncols * 5, nrows * 4))
        gs = GridSpec(nrows, ncols)

        st_txt = "This cluster has {0:.0f} customers, {1:.2%} of total population." \
            .format(self.segment_df.count()['sk_goldencustomerid'],
                    self.segment_df.count()['sk_goldencustomerid'] / self.population_df.count()['sk_goldencustomerid'])

        i = 0
        for c in range(ncols):
            for r in range(nrows):
                if i < len(profilefunclst):
                    _ax = fig.add_subplot(gs[r, c])
                    idx_df = self._profile_dataframe(profilefunclst[i])
                    self.plot_barh_ax(idx_df['index'], _ax)
                    _ax.legend().set_visible(False)
                    i += 1
                else:
                    break

        gs.tight_layout(fig)
        fig.text(0, -0.02, st_txt, fontsize='large')

        if report_path:
            from sklearn_framework.utils import save_plot
            save_plot(fig, report_path, f'{self.segment_id}_{self.dt}')
            return
        else:
            return fig


class AllSegmentsIndexingProfile(SingleSegmentIndexingProfile):
    def __init__(self, population_df, region, dt, segmentation_id_cols=None,
                 rm_ro_only_cust=False):
        super().__init__(population_df, region, dt, {'regions': region})
        self.segmentation_id_cols = segmentation_id_cols if segmentation_id_cols else ['region']
        if rm_ro_only_cust:
            _mask = ~population_df["final_segment"].isin(self.outlet_segments)
            self.population_df = population_df[_mask]

    def numericprofile(self, df, bucket, col):
        numeric_grouping = pd.cut(df[col], bucket, right=False)
        cust_cnts = df.pivot_table(index=numeric_grouping,
                                   columns=self.segmentation_id_cols,
                                   values='sk_goldencustomerid',
                                   aggfunc='count',
                                   fill_value=0)
        final = cust_cnts / cust_cnts.sum(axis=0)
        return final

    def categoricalprofile(self, df, col, orders=None):
        cust_cnts = df.pivot_table(index=col,
                                   columns=self.segmentation_id_cols,
                                   values='sk_goldencustomerid',
                                   aggfunc='count',
                                   fill_value=0)
        final = cust_cnts / cust_cnts.sum(axis=0)
        if orders:
            final = final.reindex(orders)
        return final

    def _profile_dataframe(self, profilefunc_str):
        profilefunc = getattr(self, profilefunc_str)
        segment = profilefunc(self.segment_df)
        segment.columns = [''.join(col).strip() for col in segment.columns.values]  # flatten multi-index
        return segment

    def profile_text_report(self, profilefunclst, report_path=None):
        from sklearn_framework.utils import make_dir_if_not_exist
        for profilefunc_str in profilefunclst:
            logger.info(f"[AllSegmentsIndexingProfile.profile_text_report] Running {profilefunc_str}")

            idx_df = self._profile_dataframe(profilefunc_str)
            population_df = AllSegmentsIndexingProfile(self.population_df, self.region, self.dt, ['regions'])\
                ._profile_dataframe(profilefunc_str)
            result = pd.concat([population_df, idx_df], axis=1)

            if report_path:
                report_path = make_dir_if_not_exist(report_path)
                result.fillna(0.).to_csv(os.path.join(report_path,
                                                      f'index_breakdown_{self.segment_id}_{profilefunc_str}_{self.dt}.csv'))
            else:
                print(result.to_string())
