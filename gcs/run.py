import yaml
import pandas as pd
import argparse
import os
import logging
from datetime import date
logger = logging.getLogger(__name__)


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def sysargs_parser(arg_lst):
    parser = argparse.ArgumentParser()

    # essential configs for running

    parser.add_argument('--dt', type=str, action='store', dest='dt', default=date.today().__str__(),
                        help="Date string for the refresh date. [Default] today's date.")
    parser.add_argument('--logfilename', type=str, action='store', dest='logfile_name', default="gcs_refresh.log",
                        help="The file location for the log, under the current working directory or absolute path.")

    # configs for filepath
    parser.add_argument('--home_dir', type=str, action='store', dest='home_dir', default='.',
                        help="Parent location where the program runs and all sub-folders created within. "
                             "[Default] current location.")
    parser.add_argument('--output_dir', type=str, action='store', dest='output_dir', default='output',
                        help="Relative location with respect to home_dir, indicating where to save all the "
                             "results/scores/profiles. [Default] 'output'.")
    parser.add_argument('--segmentation_files_output_dir', type=str, action='store',
                        dest='segmentation_files_output_dir', default='segmentation',
                        help="Relative location with respect to output_dir, indicating where to save all the "
                             "segmentation files. [Default] 'segmentation'.")
    parser.add_argument('--profile_files_output_dir', type=str, action='store',
                        dest='profile_files_output_dir', default='profilereports',
                        help="Relative location with respect to output_dir, indicating where to save all the "
                             "profile report files. [Default] 'profilereports'.")

    # optional configs for partial running
    parser.add_argument('--regions', nargs='+', action='store', dest='regions', default=['NA', 'EU', 'AS', 'JP'],
                        help="A list of global regions to be included in the refresh. [Values] a list within"
                             " 'NA', 'EU', 'AS', 'JP'. [Default] all regions.")
    parser.add_argument('--run_profile', type=str2bool, action='store',
                        dest='run_profile', default=True,
                        help="Whether or not to run profiling of segments. [Values] True/False. "
                             "[Default] True.")
    parser.add_argument('--standalone_profile', type=str2bool, action='store',
                        dest='standalone_profile', default=False,
                        help="Whether or not to run standalone profile. Use when have a different profile "
                             "to be used for calculation than the one used in segmentation. "
                             "[Values] True/False. [Default] False.")
    parser.add_argument('--all_indexing_profile', type=str2bool, action='store',
                        dest='all_indexing_profile', default=False,
                        help="Whether or not to create image indexing profile of certain KPIs for each segment."
                             " [Values] True/False. [Default] False.")
    parser.add_argument('--index_breakdown_profile', type=str2bool, action='store',
                        dest='index_breakdown_profile', default=False,
                        help="Whether or not to create text breakdown report for certain profile under "
                             "profiledetaillst. [Values] True/False. [Default] False.")

    # parsing args
    args = parser.parse_args(arg_lst)

    # converting all paths to absolute path
    args.output_dir = os.path.join(os.path.realpath(args.home_dir), args.output_dir)
    args.segmentation_files_output_dir = os.path.join(args.output_dir, args.segmentation_files_output_dir)
    args.profile_files_output_dir = os.path.join(args.output_dir, args.profile_files_output_dir)

    # converting to a dict
    default_configs = vars(args)
    logger.info(f"[func sysargs_parser] Running as below configs:\n{default_configs}")

    return default_configs


def segmentation(default_configs):
    from gcs.gcs_segments import GCSegmentation

    # read seg/profile configs
    with open(os.path.join('configs', f'segmentation_config.yaml')) as file:
        seg_configs = yaml.safe_load(file)
    seg_configs.update(default_configs)

    logger.info(f"[func segmentation] Running on below configs: {seg_configs}")

    for region in default_configs['regions']:
        try:
            logger.info(f"[func segmentation] Creating segmentation for {region}...")
            gcs = GCSegmentation(seg_configs, region)
            gcs.save_minisegments()
            if seg_configs['run_profile']:
                try:
                    profiling(gcs, seg_configs)
                except Exception as e:
                    logger.error(f"[func segmentation] {region} profiling failed with error: {e}.", exc_info=True)
        except Exception as e:
            logger.error(f"[func segmentation] {region} segmentation failed with error: {e}.", exc_info=True)

    return


def profiling(gcs, configs):
    """
    :param gcs: a GCSegmentation object
    :param configs: configs for running profiling
    :return:
    """
    from gcs.gcs_profile import SingleSegmentIndexingProfile, AllSegmentsStatsProfile, AllSegmentsIndexingProfile
    from collections import OrderedDict
    from sklearn_framework.utils import make_dir_if_not_exist

    report_path_miniseg = make_dir_if_not_exist(os.path.join(configs['profile_files_output_dir'], gcs.region))
    profilefunclst = configs['profilefunclst']
    profiledetaillst = configs['profiledetaillst']
    region_minisegment_size = configs['region_minisegment_size']
    df = gcs.seg_df
    region = gcs.region
    dt = gcs.dt

    # === STATS ===
    # MINI-SEGMENTS STATS
    logger.info(f"[func profiling] Creating Mini-Segment Profiles Stats for {region} {dt}...")
    AllSegmentsStatsProfile(df, region, dt, ['final_segment']) \
        .profile_stats_report(report_path_miniseg)

    if configs['index_breakdown_profile']:
        # SPECIFIC BREAKDOWN OF INDEXING
        logger.info(f"[func profiling] Creating Specific Indexing Profiles Stats for {region} {dt}...")
        AllSegmentsIndexingProfile(df, region, dt, ['final_segment'], rm_ro_only_cust=True) \
            .profile_text_report(profiledetaillst, report_path_miniseg)

    # if configs['all_indexing_profile']:
    #     # === INDEXING ===
    #     # MINI-SEGMENTS INDEXING PROFILE
    #     logger.info(f"[func profiling] Creating Mini-Segment Indexing Profiles for {region} {dt}...")
    #     for sc in ['I', 'II', 'III', 'IV']:
    #         sz = region_minisegment_size[region][sc]
    #         for i in range(1, sz + 1):
    #             SingleSegmentIndexingProfile(df, region, dt, OrderedDict(segment=sc, mini_segment=str(i))) \
    #                 .profile_indexing_graph_report(profilefunclst, report_path_miniseg)

    return


def standalone_profile(default_configs):
    from gcs.gcs_profile import SingleSegmentIndexingProfile, AllSegmentsStatsProfile, AllSegmentsIndexingProfile
    from collections import OrderedDict
    from sklearn_framework.utils import make_dir_if_not_exist

    def prep_df(region, dt):
        feat_path = os.path.join(seg_configs['rawdata_dir'], seg_configs['feature_file_name'])
        seg_path = os.path.join(seg_configs['segmentation_files_output_dir'], f'{region}_segmentation_{dt}.csv')
        prof_path = os.path.join(seg_configs['rawdata_dir'], seg_configs['profile_file_name'])

        segmentation_df = pd.read_csv(seg_path, **seg_configs['filereadingparams'])
        feat_df = pd.read_csv(feat_path, **seg_configs['filereadingparams']) \
            .fillna(seg_configs['na_replacement_value'])
        profile_df = pd.read_csv(prof_path, **seg_configs['filereadingparams']) \
            .fillna(seg_configs['na_replacement_value'])

        _f, _p, _s = feat_df.loc[feat_df['regions'] == region, seg_configs['feature_cols_needed']], \
                     profile_df.loc[profile_df['regions'] == region, :], \
                     segmentation_df

        final = pd.concat([
            _f.set_index(['sk_goldencustomerid']),
            _p.set_index(['sk_goldencustomerid']).drop('regions', axis=1),
            _s.set_index(['sk_goldencustomerid']).drop('regions', axis=1),
        ], axis=1, join='inner').reset_index()

        return final

    # read seg/profile configs
    with open(os.path.join('configs', f'segmentation_config.yaml')) as file:
        seg_configs = yaml.safe_load(file)
    seg_configs.update(default_configs)

    profilefunclst = seg_configs['profilefunclst']
    profiledetaillst = seg_configs['profiledetaillst']
    region_minisegment_size = seg_configs['region_minisegment_size']
    dt = seg_configs['dt']

    for region in seg_configs['regions']:
        report_path_miniseg = make_dir_if_not_exist(os.path.join(seg_configs['profile_files_output_dir'], region))
        df = prep_df(region, dt)

        # === STATS ===
        # MINI-SEGMENTS STATS
        logger.info(f"[func standalone_profile] Creating Mini-Segment Profiles Stats for {region} {dt}...")
        AllSegmentsStatsProfile(df, region, dt, ['final_segment']) \
            .profile_stats_report(report_path_miniseg)

        if seg_configs['index_breakdown_profile']:
            # SPECIFIC BREAKDOWN OF INDEXING
            logger.info(f"[func standalone_profile] Creating Specific Indexing Profiles Stats for {region} {dt}...")
            AllSegmentsIndexingProfile(df, region, dt, ['final_segment']) \
                .profile_text_report(profiledetaillst, report_path_miniseg)

        if seg_configs['all_indexing_profile']:
            # === INDEXING ===
            # MINI-SEGMENTS INDEXING PROFILE
            logger.info(f"[func standalone_profile] Creating Mini-Segment Indexing Profiles for {region} {dt}...")
            for sc in ['I', 'II', 'III', 'IV']:
                sz = region_minisegment_size[region][sc]
                for i in range(1, sz + 1):
                    SingleSegmentIndexingProfile(df, region, dt, OrderedDict(segment=sc, mini_segment=str(i))) \
                        .profile_indexing_graph_report(profilefunclst, report_path_miniseg)

    return
