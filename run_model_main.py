from sklearn_framework.preprocessing import *
from sklearn_framework.run import model_run, sysargs_parser
import os
import logging


def modeling(configs):
    churn_scored_path = model_run(configs, "churn", os.path.join("configs", "churn_config.yaml"))
    logger.info(f"[func segmentation] Churn files saved at {churn_scored_path}.")
    mb_scored_path = model_run(configs, "mb",  os.path.join("configs", "mb_config.yaml"))
    logger.info(f"[func segmentation] MB files saved at {mb_scored_path}.")
    return churn_scored_path, mb_scored_path


if __name__ == "__main__":
    import sys

    # getting default configs
    default_configs = sysargs_parser(sys.argv[1:])

    logfile_name = default_configs["logfile_name"]
    # setup logger
    logFormatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=logFormatter, level=logging.INFO)
    logger = logging.getLogger()
    # os.remove(logfile_name) if os.path.exists(logfile_name) else None

    # add file handler
    handler = logging.FileHandler(logfile_name)
    handler.setFormatter(logging.Formatter(logFormatter))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    # running scoring/training etc.
    logger.info("=" * 30)
    logger.info("RUNNING run_model_main.py ...")

    modeling(default_configs)
    logger.info("DONE!")
