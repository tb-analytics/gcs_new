set -e

dtstring=$1
homedir="/home/cdsw"
# homedir='.'  # in source
logfile="${homedir}/log/gcs_refresh_$(date +"%Y%m%d%H%M%S").log"

# refresh the data and download features
 python3 ${homedir}/churn_mb_dataprep/cml_refresh/exec_hql_queries.py \
  --filename ${homedir}/churn_mb_dataprep/sql/features.hql \
  --queryargs "CURRENT_DATE='${dtstring}'" \
  --logfilename $logfile &&
 python3 ${homedir}/churn_mb_dataprep/cml_refresh/download_hive_table_to_csv.py \
  --logfilename $logfile \
  --table_name "tb_analytics.dy_gcs_feat_final" \
  --dest_name ${homedir}/datasets/features_$dtstring.csv &&

# refresh the data and download profile
 python3 ${homedir}/churn_mb_dataprep/cml_refresh/exec_hql_queries.py \
  --filename ${homedir}/churn_mb_dataprep/sql/profile.hql \
  --queryargs "CURRENT_DATE='${dtstring}'" "CHANNEL_STRING='EC', 'RF', 'RO'" \
  --logfilename $logfile &&
 python3 ${homedir}/churn_mb_dataprep/cml_refresh/download_hive_table_to_csv.py \
  --logfilename $logfile \
  --table_name tb_analytics.dy_gcs_profile_final \
  --dest_name ${homedir}/datasets/profile_$dtstring.csv &&

# downloading FPA scores
hadoop fs -copyToLocal -f abfs://data@clouderastorageaccountpd.dfs.core.windows.net/tmp/fpa2gcs/*.csv ~/output/scored/fpa

# run modeling
python3 ${homedir}/run_model_main.py \
    --home_dir ${homedir} \
    --rawdata_dir datasets \
    --feature_file_name features_$dtstring.csv \
    --logfilename $logfile &&

# run profiling
python3 ${homedir}/gcs_main.py --dt ${dtstring} \
   --home_dir ${homedir} \
   --index_breakdown_profile True \
   --logfilename $logfile &&

# update excel reports
python3 ${homedir}/update_excel_report.py --dt ${dtstring} \
  --profile_folder ${homedir}/output/profilereports \
  --logfilename $logfile &&

# update end tables
python3 ${homedir}/churn_mb_dataprep/cml_refresh/update_end_tables.py --dt $dtstring \
    --logfilename $logfile \
    --skip_sanity_check True

# zip desired files and send
rm -f ${homedir}/results.zip &&
zip -r -j ${homedir}/results.zip $logfile \
    ${homedir}/output/profilereports/excel_reports/*/*_$dtstring.xlsx \
    ${homedir}/output/segmentation/AS_segmentation_$dtstring.csv \
    ${homedir}/output/segmentation/EU_segmentation_$dtstring.csv

echo "==============================" >> $logfile
echo "GCS Refresh Success!" >> $logfile
