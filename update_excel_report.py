import openpyxl as opx
import pandas as pd
import argparse
import gcs
import os
import sys
import logging


def make_dir_if_not_exist(pathdir):
    if not os.path.exists(pathdir):
        os.makedirs(pathdir)
    return pathdir


def is_less_than_1yr(dt_str):
    from datetime import date
    today = date.today()
    refresh_dt = date(*(int(ele) for ele in dt_str.split('-')))
    days_diff = (today - refresh_dt).days
    return days_diff < 365


def excel_cell_reference_to_rc(cell_name):
    import re

    cell_name = cell_name.upper()

    if not re.match("^[A-Z]+[1-9]+[0-9]*$", cell_name):
        logger.error("[func excel_cell_reference_to_rc] Incorrect Excel cell reference, should be something like A1.")
        raise ValueError()

    column_letter, row_num_string = re.search("^([A-Z]+)([1-9]+[0-9]*)$", cell_name).groups()

    row_num = int(row_num_string)
    col_num = 0
    for i, c in enumerate(column_letter[::-1]):
        col_num += (ord(c) - 64) * 26 ** i
    return row_num, col_num


def assign_df_to_cell(worksheet, df, cell_name):
    from openpyxl.utils.dataframe import dataframe_to_rows
    r, c = excel_cell_reference_to_rc(cell_name)
    _df = df.reset_index()
    for row in dataframe_to_rows(_df, index=False, header=True):
        _c = c
        for val in row:
            worksheet.cell(row=r, column=_c).value = val
            _c += 1
        r += 1
    return


def update_excel_report(xlsx_filename, region, dt, _df):
    # ==== update the excel reports ===
    # read the workbooks

    def _paste_index_breakdownfile(breakdown_profile_name, dest_excel_cell):
        _filename = os.path.join(datafiles_path, region, f"index_breakdown_{region}_{breakdown_profile_name}_{dt}.csv")
        _df = pd.read_csv(_filename, index_col=0, keep_default_na=False)
        assign_df_to_cell(wb[raw_sheet_dict[region]], _df, dest_excel_cell)

    wb = opx.load_workbook(xlsx_filename)
    logger.info(f"[func update_excel_report] Updating {region} ...")

    # remove next r12m data
    if is_less_than_1yr(dt):
        _df.loc[['actual_retention_rate', 'actual_mb_rate'], :] = None

    # update the sheet
    assign_df_to_cell(wb[raw_sheet_dict[region]], _df, "A1")
    wb[prof_sheet_dict[region]]["A1"].value = f"Date: {dt}"

    # if NA update the mosaic
    if region == 'NA':
        _paste_index_breakdownfile("mosaicprofile", "A70")
        _paste_index_breakdownfile("agegroupprofile", "A100")

    if region in ('NA', 'EU', 'JP'):
        _paste_index_breakdownfile("channelbreakdownprofile", "A125")

    _paste_index_breakdownfile("netsalesprofile", "A140")
    _paste_index_breakdownfile("visitsprofile", "A150")

    # wb.save(xlsx_filename)
    # logger.info(f'[func update_excel_report] {region} template saved at {xlsx_filename}!')

    date_report_path = os.path.join(make_dir_if_not_exist(os.path.join(datafiles_path, "excel_reports", region)),
                                    f"segprofile_{region.lower()}_{dt}.xlsx")
    wb.save(date_report_path)
    logger.info(f'[func update_excel_report] {region} report saved at {date_report_path}!')
    
    wb.close()
    logger.info(f'[func update_excel_report] {region} processed!')

    return


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--dt', type=str, action='store', dest='dt', required=True)
    parser.add_argument('--profile_folder', type=str, action='store', dest='profile_folder')
    parser.add_argument('--logfilename', type=str, action='store', dest='logfile_name', default="gcs_refresh.log")

    args = parser.parse_args(sys.argv[1:])
    dt = args.dt
    logfile_name = args.logfile_name
    template_path = os.path.join(os.path.dirname(gcs.__file__), "excel_report_template")
    datafiles_path = args.profile_folder

    # setup logger
    logFormatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=logFormatter, level=logging.INFO)
    logger = logging.getLogger()
    # os.remove(logfile_name) if os.path.exists(logfile_name) else None

    # add file handler
    handler = logging.FileHandler(logfile_name)
    handler.setFormatter(logging.Formatter(logFormatter))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    logger.info("=" * 30)
    logger.info("RUNNING update_excel_report.py ...")

    regions = ['NA', 'EU', 'AS', 'JP']
    raw_sheet_dict = {rg: rg.lower() + '_raw' for rg in regions}
    prof_sheet_dict = {rg: rg + ' MINISEG CURRENT' for rg in regions}

    for region in regions:
        xlsx_filename = os.path.join(template_path, f"segprofile_{region.lower()}_new.xlsx")
        profile_csv_filename = os.path.join(datafiles_path, region, "report_source_csv",
                                            f"{region}_segmentprofile_{dt}.csv")
        profile_raw = pd.read_csv(profile_csv_filename, index_col=0, keep_default_na=False)

        try:
            update_excel_report(xlsx_filename, region, dt, profile_raw.copy())
        except Exception as e:
            logger.error(e, exc_info=True)
