from gcs.run import segmentation, sysargs_parser
import os
import logging


if __name__ == "__main__":
    import sys

    # getting default configs
    default_configs = sysargs_parser(sys.argv[1:])

    logfile_name = default_configs["logfile_name"]
    # setup logger
    logFormatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=logFormatter, level=logging.INFO)
    logger = logging.getLogger()
    # os.remove(logfile_name) if os.path.exists(logfile_name) else None

    # add file handler
    handler = logging.FileHandler(logfile_name)
    handler.setFormatter(logging.Formatter(logFormatter))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    # running scoring/training etc.
    logger.info("=" * 30)
    logger.info("RUNNING gcs_main.py ...")
    segmentation(default_configs)
    logger.info("DONE!")
