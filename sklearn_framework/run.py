from sklearn_framework.preprocessing import *
from sklearn_framework.modeling import train_model, score_model, validate_model
from sklearn_framework.utils import load_yaml_configs
import argparse
import os
import logging

logger = logging.getLogger(__name__)


def _filenames_handler(keyval_text: str):
    kvpairs = keyval_text.split('=')
    if len(kvpairs) == 1:
        k = kvpairs[0]
        v = '1'
    elif len(kvpairs) == 2:
        k, v = kvpairs
    else:
        raise ValueError("[func sysargs_parser._filenames_handler] Argument must be a single value or key=val.")

    assert k.endswith('.csv'), "[func sysargs_parser._filenames_handler] File must be in *.csv format."
    assert v.replace('.', '', 1).isdigit(), \
        "[func sysargs_parser._filenames_handler] Weight of a file must be a number."

    weight = float(v)
    assert 0 < weight <= 1, "[func sysargs_parser._filenames_handler] Weight of a file must be within (0, 1]."

    d = dict()
    d['filename'] = k
    d['weight'] = weight
    return d


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def sysargs_parser(arg_lst):
    parser = argparse.ArgumentParser()

    # essential configs for running
    parser.add_argument('--mode', type=str, action='store', dest='mode', default='score',
                        help="Mode for modeling behaviors. [Values] 'score', 'validate', 'train'. "
                             "[Default] 'score'.")
    parser.add_argument('--feature_file_name', type=str, action='store',
                        dest='feature_file_name',
                        help="When mode is 'score'/'validate', this is the feature file name to be used, "
                             "under the rawdata_dir.")
    parser.add_argument('--filenames', metavar="KEY=VALUE", nargs='+', dest='filenames',
                        help="When mode is 'train', filenames arg is required. If mode is 'score'/'validate', "
                             "this arg will be ignored."
                             "It's a list of key-value pairs of filename and the corresponding weight. "
                             "If weight not specified, use 1 - idx*0.2 as default, minimum 0.1."
                             "Sample: `filename1=1 filename=0.8` or `filename1 filename2`")
    parser.add_argument('--logfilename', type=str, action='store', dest='logfile_name', default="gcs_refresh.log",
                        help="The file location for the log, under the current working directory or absolute path.")

    # configs for filepath
    parser.add_argument('--home_dir', type=str, action='store', dest='home_dir', default='.',
                        help="Parent location where the program runs and all sub-folders created within. "
                             "[Default] current location.")
    parser.add_argument('--rawdata_dir', type=str, action='store', dest='rawdata_dir', default='datasets',
                        help="Relative location with respect to home_dir, indicating where to find feature "
                             "and profile files. [Default] 'datasets'.")
    parser.add_argument('--model_output_dir', type=str, action='store', dest='model_output_dir', default='models',
                        help="Relative location with respect to home_dir, indicating where to save all the "
                             "models. [Default] 'models'.")
    parser.add_argument('--output_dir', type=str, action='store', dest='output_dir', default='output',
                        help="Relative location with respect to home_dir, indicating where to save all the "
                             "results/scores/profiles. [Default] 'output'.")
    parser.add_argument('--scored_files_output_dir', type=str, action='store',
                        dest='scored_files_output_dir', default='scored',
                        help="Relative location with respect to output_dir, indicating where to save all the "
                             "ML model scores. [Default] 'scored'.")

    # parsing args
    args = parser.parse_args(arg_lst)

    # converting all paths to absolute path
    args.rawdata_dir = os.path.join(os.path.realpath(args.home_dir), args.rawdata_dir)
    args.model_output_dir = os.path.join(os.path.realpath(args.home_dir), args.model_output_dir)
    args.output_dir = os.path.join(os.path.realpath(args.home_dir), args.output_dir)
    args.scored_files_output_dir = os.path.join(args.output_dir, args.scored_files_output_dir)

    # to construct all files:
    # if train, filenames arg is required in key-val pairs. feature_file_name and profile_file_name is not required
    # if validate/score, use feature_file_name and profile_file_name, if missing: use prefix and dt

    if args.mode == 'train':
        if args.filenames is None:
            parser.error("[func sysargs_parser] Train mode requires filenames with corresponding weights.")
        else:
            args.filenames = [_filenames_handler(fs) for fs in args.filenames]
    else:
        if args.feature_file_name is None:
            parser.error("[func sysargs_parser] Score/Validate mode requires a single feature_file_name.")
        else:
            args.filenames = [{'filename': args.feature_file_name, 'weight': 1}]

    # converting to a dict
    default_configs = vars(args)
    logger.info(f"[func sysargs_parser] Running as below configs:\n{default_configs}")

    return default_configs


def model_run(default_configs, model_cat: str, config_file: str):
    configs = load_yaml_configs(config_file)
    configs.update(default_configs)
    configs['model_output_dir'] = os.path.join(configs['model_output_dir'], model_cat)
    configs['scored_files_output_dir'] = os.path.join(configs['scored_files_output_dir'], model_cat)

    score_saved_path = None
    for modelname in configs['modelspec']:
        try:
            logger.info(f"[func model_run] Processing {configs['mode']} {model_cat} {modelname}...")
            features = load_data(configs, modelname)
            if configs['mode'] == 'train':
                train, test = train_test_prep(features, configs, modelname)
                train_model(train, test, configs, modelname)
            elif configs['mode'] == 'score':
                score_saved_path = score_model(features, configs, modelname)
            elif configs['mode'] == 'validate':
                score_saved_path = validate_model(features, configs, modelname)
            logger.info(f"[func model_run] Done processing {configs['mode']} {model_cat} {modelname}!")
        except Exception as e:
            logger.error(f"[func model_run] {modelname}_{model_cat} failed with error: {e}", exc_info=True)
    return score_saved_path
