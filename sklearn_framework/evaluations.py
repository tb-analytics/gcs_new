import matplotlib.pyplot as plt
from sklearn_framework.utils import save_plot
import sklearn
import numpy as np
import pandas as pd
import logging
logger = logging.getLogger(__name__)


class Evaluation:

    def __init__(self, featureset, model):
        self.model = model
        self.featureset = featureset
        self.index = featureset.index.reset_index(drop=True)
        self.X = featureset.X.reset_index(drop=True)
        self.y = featureset.y.reset_index(drop=True)
        self.prob_col = 'pos_score'
        self.target = self.y.name
        self.id_col = featureset.index.columns[0]
        self.score = pd.DataFrame(self.model.predict_proba(self.X)[:, -1], columns=[self.prob_col])
        self.df = pd.concat([self.index, self.y, self.score], axis=1)


class ClassificationEval(Evaluation):

    def __init__(self, featureset, model):
        super().__init__(featureset, model)

    def plot_classification_evaluation(self, precisionrecall=False, savepath=None):
        if precisionrecall:
            fig, axs = plt.subplots(1, 3, figsize=(15, 4))
            sklearn.metrics.plot_precision_recall_curve(self.model, self.X, self.y, ax=axs[2])
            axs[2].set_title('Precision-Recall Curve')
        else:
            fig, axs = plt.subplots(1, 2, figsize=(10, 4))

        sklearn.metrics.plot_confusion_matrix(self.model, self.X, self.y,
                                              normalize="true",
                                              display_labels=['0', '1'],
                                              cmap='Blues', ax=axs[0])
        sklearn.metrics.plot_roc_curve(self.model, self.X, self.y, ax=axs[1])
        axs[0].set_title('Confusion Matrix')
        axs[1].set_title('ROC Curve')

        save_plot(fig, savepath, 'classification_eval')
        return

    def confusion_matrix_all_thresholds(self, class_of_interest=1):
        from sklearn.metrics import confusion_matrix
        y_true = self.y
        y_pred_prob_all_classes = self.model.predict_proba(self.X)
        # look at class label of interest
        y_pred_prob = y_pred_prob_all_classes[:, self.model.classes_ == class_of_interest].ravel()

        res = list()

        for threshold in np.linspace(0.01, 0.99, 99):
            y_pred = (y_pred_prob >= threshold).astype(int)
            cm = confusion_matrix(y_true, y_pred, normalize=None)
            res.append(pd.DataFrame.from_dict({
                threshold: cm.ravel()
            }, orient='index'))

        # concatenating all results at each threshold
        cm_df = pd.concat(res)
        cm_df.columns = ['tn', 'fp', 'fn', 'tp']
        cm_df['precision'] = cm_df['tp'] / (cm_df['tp'] + cm_df['fp'])
        cm_df['fp - fn'] = cm_df['fp'] - cm_df['fn']
        cm_df['accuracy'] = (cm_df['tp'] + cm_df['tn']) / (cm_df['tp'] + cm_df['tn'] + cm_df['fp'] + cm_df['fn'])
        cm_df['err_rate'] = cm_df['fp - fn'] / (cm_df['tp'] + cm_df['tn'] + cm_df['fp'] + cm_df['fn'])

        return cm_df.reset_index()


class CumGainLiftEval(Evaluation):

    def __init__(self, featureset, model, step=5):
        super().__init__(featureset, model)
        self.step = step
        self.lgtable, self.avg_target = self._gain_lift_analysis()
        
    def _gain_lift_analysis(self):
        """
        Gain: 0 ~ 1, percentage of true positives within the bucket
        Lift: 1 ~ inf, how much better the bucket collects true positives than random choose 
        """
        _df = self.df.copy()
        _df['bucket'] = np.floor(_df[self.prob_col].rank(pct=True, ascending=False) * 99.999 / self.step)
        lgtable = _df.groupby('bucket').agg(
            bucketcnts=(self.id_col, 'count'),
            targetcnts=(self.target, 'sum'),
            upperbound_prob=(self.prob_col, 'max'),
            lowerbound_prob=(self.prob_col, 'min')
        ).reset_index()

        lgtable['percentile'] = (lgtable['bucket'] + 1) * self.step
        lgtable['pospct'] = lgtable['targetcnts'] / lgtable['bucketcnts']
        lgtable['cumtarget'] = lgtable['targetcnts'].cumsum()
        lgtable['cumbucket'] = lgtable['bucketcnts'].cumsum()
        lgtable['gain'] = lgtable['cumtarget'] / lgtable['targetcnts'].sum()
        lgtable['cumlift'] = lgtable['gain'] * lgtable['bucketcnts'].sum() / lgtable['cumbucket']  #derived form of lift

        avg_target = lgtable['targetcnts'].sum() / lgtable['bucketcnts'].sum() 

        return lgtable, avg_target
    
    def plot_cumulative_gain(self, savepath=None):
        fig, ax = plt.subplots(1, 1, figsize=(5, 4))
        x = np.insert(self.lgtable['percentile'].values, 0, 0)  # adding (0,0)
        y = np.insert(self.lgtable['gain'].values, 0, 0)  # adding (0,0)

        ax.plot(x, y, marker='o', label='Model')
        ax.plot([0, 100], [0, 1], label='Random Guess', color='orange', linewidth=1)
        ax.plot([0, self.avg_target*100, 100], [0, 1, 1], label='Perfect', color='green', linewidth=1, linestyle='--')
        ax.set_title('Gain')
        ax.legend()
        ax.set_xlabel('Percentile')
        ax.set_ylabel('Gain')

        save_plot(fig, savepath, 'cumgain_eval')
        return

    def plot_cumulative_lift(self, savepath=None):
        fig, ax = plt.subplots(1, 1, figsize=(5, 4))
        _remainingx = np.arange(np.ceil(self.avg_target*10) * 10, 101, step=self.step)
        _remainingy = 100 / _remainingx
        ideallift_x = np.concatenate((np.array([0, self.avg_target*100]), _remainingx))
        ideallift_y = np.concatenate((np.array([1/self.avg_target, 1/self.avg_target]), _remainingy))

        ax.plot(self.lgtable['percentile'], self.lgtable['cumlift'], marker='o', label='Model')
        ax.plot([0, 100], [1, 1], label='Random Guess', color='orange', linewidth=1)
        ax.plot(ideallift_x, ideallift_y, label='Perfect', color='green', linewidth=1, linestyle='--')
        ax.set_title('Cum. Lift')
        ax.legend()
        ax.set_xlabel('Percentile')
        ax.set_ylabel('Cum. Lift')

        save_plot(fig, savepath, 'cumlift_eval')
        return


class PermutationFeatureImportance(Evaluation):

    def __init__(self, featureset, model):
        super().__init__(featureset, model)
        self.result = self._feature_importantce()

    def _feature_importantce(self, scoring='roc_auc'):
        from sklearn.inspection import permutation_importance
        result = permutation_importance(self.model, self.X, self.y, n_repeats=10,
                                        random_state=42, scoring=scoring, n_jobs=2)
        return result

    def permutation_importance(self):
        feat_imp = self.result.importances_mean
        corrs = [round(self.X[col].corr(self.y), 4) for col in self.X.columns]

        _report = pd.DataFrame([feat_imp, corrs], columns=self.X.columns, index=['feat_imp', 'corr_values']).T
        _report = _report.sort_values('feat_imp', ascending=False)
        return _report

    def plot_permutation_importance(self, savepath=None, topk=10):
        perm_sorted_idx = self.result.importances_mean.argsort()
        perm_sorted_idx = perm_sorted_idx[-topk:]

        _labels = ['(+)' + col if self.X[col].corr(self.y) > 0 else '(-)' + col for col in
                   self.X.columns[perm_sorted_idx]]

        fig, ax = plt.subplots(1, 1, figsize=(7, 5))
        ax.boxplot(self.result.importances[perm_sorted_idx].T, vert=False,
                   labels=_labels)
        ax.set_title(f'Permutation Feature Importance')
        fig.tight_layout()

        save_plot(fig, savepath, 'perm_feature_importance')
        return

    def plot_permutation_importance_multicollinearity_removed(self, *args, **kwargs):
        from sklearn.base import clone
        # get the same estimator of the model, but re-fit on a subset of features
        _model = clone(self.model)
        _featureset = self.featureset.remove_multicollinearity()
        _model.fit(_featureset.X, _featureset.y)
        PermutationFeatureImportance(_featureset, _model).plot_permutation_importance(*args, **kwargs)
        return


"""
---------------------
      FUNCTIONS
---------------------
"""


def plot_feature_correlations(corrdf, savepath=None):
    from scipy.cluster import hierarchy
    corr = corrdf.values

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 8))
    corr_linkage = hierarchy.ward(corr)
    dendro = hierarchy.dendrogram(
        corr_linkage, labels=corrdf.columns.tolist(), ax=ax1, leaf_rotation=90
    )
    dendro_idx = np.arange(0, len(dendro['ivl']))

    ax2.imshow(corr[dendro['leaves'], :][:, dendro['leaves']])
    ax2.set_xticks(dendro_idx)
    ax2.set_yticks(dendro_idx)
    ax2.set_xticklabels(dendro['ivl'], rotation='vertical')
    ax2.set_yticklabels(dendro['ivl'])
    fig.tight_layout()
    save_plot(fig, savepath, 'feature_correlations')

    return corr_linkage
