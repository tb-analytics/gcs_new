from pyspark.sql import SparkSession
import time
import re
import traceback
import sys
import os
import argparse
from datetime import date
import logging


def exec_multiple_queries(query_script):
    queries = [q.strip() for q in query_script.split(";") if q.strip()]
    for q in queries:
        if q.lower().startswith("drop table"):
            spark.sql(q)
            logger.info("[func exec_multiple_queries] ==== {0:.2f} seconds passed ====".format(time.time() - start_time))
            logger.info(f"[func exec_multiple_queries] {q} finished.")
        else:
            logger.info("[func exec_multiple_queries] ==== {0:.2f} seconds passed ====".format(time.time() - start_time))
            
            try:
                tbl_name = re.findall("create table ([a-z1-9._]+) as\\n", q, flags=re.I)[0]
                _query = re.sub("create table [a-z1-9._]+ as\\n", "", q, flags=re.I)
            except Exception:
                logger.error(f"[func exec_multiple_queries] Error executing {q}", exc_info=True)
                raise ValueError()

            # create table
            logger.info(f"[func exec_multiple_queries] ... Creating {tbl_name} ...")

            df = spark.sql(_query)
            df.write.saveAsTable(tbl_name, mode="overwrite")
    return


def prep_queries(file, queryargs):
    with open(file, 'r') as f:
        query = "".join(f.readlines())
    query = query.replace("$", "").format(**queryargs)
    return query


def split_queryargs(keyval_list):
    kvpairs = [keyval_text.split('=') for keyval_text in keyval_list]
    query_args = {k: v for k, v in kvpairs}
    return query_args


if __name__ == "__main__":
    spark = SparkSession \
            .builder \
            .enableHiveSupport() \
            .appName("exec_hql_queries") \
            .getOrCreate()
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--logfilename', type=str, action='store', dest='logfile_name', default="gcs_refresh.log")
    parser.add_argument('--filename', type=str, action='store', dest='filename')
    parser.add_argument('--queryargs', metavar="KEY=VALUE", nargs='+', dest='queryargs')
    args = parser.parse_args()

    filename = args.filename
    logfile_name = args.logfile_name

    # query args
    if args.queryargs is not None:
        queryargs = split_queryargs(args.queryargs)
    else:
        queryargs = dict()

    # setup logger
    logFormatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=logFormatter, level=logging.INFO)
    logger = logging.getLogger()
    # os.remove(logfile_name) if os.path.exists(logfile_name) else None

    # add file handler
    handler = logging.FileHandler(logfile_name)
    handler.setFormatter(logging.Formatter(logFormatter))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    logger.info("=" * 30)
    logger.info("RUNNING exec_hql_queries.py ...")

    # start to run
    start_time = time.time()
    logger.info("[func main] ==== Refresh datasets ending on {0} ====".format(queryargs))
    logger.info("[func main] ==== {0:.2f} seconds passed ====".format(time.time() - start_time))

    try:
        exec_multiple_queries(prep_queries(filename, queryargs))
    except Exception as e:
        logger.error(e, exc_info=True)
    
    logger.info("[func main] ==== {0:.2f} seconds passed ====".format(time.time() - start_time))
    logger.info("[func main] All finished!")
    
    spark.stop()
