# Setting up Spark
from pyspark.sql import SparkSession, DataFrame
from pyspark.sql.types import *
from pyspark import StorageLevel
import pyspark.sql.functions as f

import os
import time
import datetime
import traceback
import sys
import argparse
from functools import reduce
import pandas as pd
import logging


"""
===== CONSTANTS =====
"""
HADOOP_FILE_PATH = "file:///home/cdsw/output/segmentation"
PROFILE_REPORTS_PATH = "/home/cdsw/output/profilereports"

# stg1_table_name = "cml_staging.gcs_partyhistory_stg"
# stg1_dedup_table_name = "cml_staging.gcs_partyhistory_stg_dedup"
# stg2_table_name = "cml_staging.DimGCSGoldenCustomerGCSHistory"
# current_score_table_name = "cml_staging.DimGCSCurrentScores"
# profile_history_table_name = "cml_staging.DimGCSProfileHistory"

stg1_table_name = "tb_analytics.dy_gcs_partyhistory_stg1_new"
stg1_dedup_table_name = "tb_analytics.dy_gcs_partyhistory_stg1_dedup_new"
stg2_table_name = "tb_analytics.dy_gcs_partyhistory_stg2_new"
current_score_table_name = "tb_analytics.dy_gcs_current_scores_new"
profile_history_table_name = "tb_analytics.dy_gcs_profile_history_new"

REGIONS = ('NA', 'EU', 'AS', 'JP')

REFRESH_FILE_COLS = [
    'sk_goldencustomerid',
    'region',
    'sk_partyid',
    'segment',
    'ret_score',
    'mb_score',
    'fpa_score',
    'refresh_dt'
]

STAGING1_COLS = [
    'region',
    'sk_partyid',
    'segment',
    'ret_score',
    'mb_score',
    'fpa_score',
    'refresh_dt'
]

SCHEMA_SCORED = StructType([
    StructField("sk_goldencustomerid", IntegerType(), True),
    StructField("region", StringType(), False),
    StructField("segment", StringType(), True),
    StructField("ret_score", DoubleType(), True),
    StructField("mb_score", DoubleType(), True),
    StructField("fpa_score", DoubleType(), True),
])

fields_to_keep = ['region', 'profile_dt', 'segment', 'custcnt', 'regionalpct', 'regionalpct_fpc',
                'r12m_netsalesamt_total', 'regionalsalespct', 'regionalsalespct_fpc',
                'churn_pos_prob', 'mb_pos_prob', 'r12m_new_rate', 'r12m_react_rate',
                'r12m_retained_rate', 'actual_retention_rate', 'actual_mb_rate',
                'r12m_rf_rate', 'r12m_ec_rate', 'r12m_ro_rate', 'r12m_omnichannel_rate',
                'r12m_multicategory_rate', 'r12m_majorcatshopped',
                'diff_country_tourists_rate', 'diff_region_tourists_rate',
                'shopped_rtw_rate', 'shopped_slg_rate', 'shopped_shoes_rate',
                'shopped_handbags_rate', 'shopped_jewelry_rate', 'shopped_sport_rate',
                'rtw_newness_med', 'sport_newness_med', 'handbags_newness_med',
                'shoes_newness_med', 'jewelry_newness_med', 'slg_newness_med',
                'r12m_rtw_fp_pct', 'r12m_slg_fp_pct', 'r12m_shoes_fp_pct',
                'r12m_handbags_fp_pct', 'r12m_jewelry_fp_pct', 'r12m_sport_fp_pct',
                'r12m_netsalesamt', 'r12m_avgsalesmkdnrate', 'r12m_avgsalesdsctrate',
                'r12m_avgfullpricerate_dollars', 'r12m_avgfullpricerate_units',
                'r12m_bought_fp_only_itm_rate', 'r12m_bought_md_only_itm_rate',
                'r12m_bought_dsct_only_itm_rate', 'r12m_bought_md_x_dsct_itm_rate',
                'r12m_fp_item_rate', 'r12m_md_item_rate', 'r12m_dsct_item_rate',
                'r12m_md_x_dsct_item_rate', 'r12m_fp_only_rate',
                'r12m_offprice_only_rate', 'r12m_mix_price_rate', 'r12m_visits',
                'r12m_one_timer_rate', 'r12m_aov', 'r12m_aur', 'r12m_upt',
                'marketable_rate', 'emailable_rate', 'phoneable_rate', 'mailable_rate',
                'ltv']


"""
===== FUNCTIONS =====
"""


def unionAll(*dfs):
    return reduce(DataFrame.unionAll, dfs)


def save_table(df, table_name, format="hive", mode="overwrite"):
    df.write \
        .format(format) \
        .mode(mode) \
        .saveAsTable(table_name)
    return


def read_table(table_name):
    df = spark.read.table(table_name)
    return df

def table_exists(table_name):
    return spark._jsparkSession.catalog().tableExists(*table_name.split('.'))


def dtstring_to_date(dt):
    return datetime.datetime(*[int(s) for s in dt.split('-')])


def load_segmentation_files(dt, regions=REGIONS):
    tmp_holder = list()
    for rg in regions:
        try:
            _df = spark.read.format("com.databricks.spark.csv") \
                .options(header='true') \
                .load('{hdp_file_path}/{rg}_segmentation_{dt}.csv'.format(rg=rg, dt=dt,
                                                                          hdp_file_path=HADOOP_FILE_PATH)
                      , schema=SCHEMA_SCORED)
            tmp_holder.append(_df)
        except Exception as e:
            logger.error(e, exc_info=True)
            continue

    df = unionAll(*tmp_holder)
    return df


def sanity_check_for_counts(dt, regions=REGIONS, n_sigma=5):
    logger.info("[func sanity_check_for_counts] Checking to see if customer counts per segment are reasonable ...")

    threshold_df = spark.sql('''
    with tmp as (
    select region, segment, profile_dt, custcnt
      , lead(custcnt) over (partition by region, segment order by profile_dt) as next_cnts
      , lead(profile_dt) over (partition by region, segment order by profile_dt) as next_dt
      , rank() over (partition by region, segment order by profile_dt desc) as latest_record
    from {PROF_HIST_TABLE}
    )
    , tmp2 as (
    select region, segment, profile_dt
      , next_cnts - custcnt as cntsdiff
      , ( next_cnts - custcnt) / custcnt as cntsdiffpct
    from tmp
    where next_dt is not null
      and profile_dt >= add_months('{CURRENT_DATE}', -6)
    )
    , lastest_record as (
    select region, segment, profile_dt, custcnt
    from tmp
    where latest_record = 1
    )
    , threshold as (
    select t.region, t.segment
      , avg(cntsdiffpct) + {N_SIGMA} * stddev(cntsdiffpct) upper_ci
      , avg(cntsdiffpct) - {N_SIGMA} * stddev(cntsdiffpct) lower_ci
    from tmp2 t
    group by t.region, t.segment
    )
    select t.region, t.segment
      , greatest(0, floor(l.custcnt * (1 + lower_ci))) as lower_cnt
      , ceil(l.custcnt * (1 + upper_ci)) as upper_cnt
    from threshold t
    join lastest_record l
      on t.region = l.region
      and t.segment = l.segment
    where t.region <> l.segment
    '''.format(N_SIGMA=n_sigma, CURRENT_DATE=dt, PROF_HIST_TABLE=profile_history_table_name))
    
    if threshold_df.count() == 0:
        logger.warning("[func sanity_check_for_counts] No threshold counts fetched, will continue updating ...")
    
    df = load_segmentation_files(dt, regions)
    curr_cnts = df.groupBy(['region', 'segment']).count()

    # the counts for this round should be within n-sigma of stddev of last r6M
    checking_df = threshold_df.join(curr_cnts, ['region', 'segment']) \
        .withColumn('checking', f.col('count').between(f.col('lower_cnt'), f.col('upper_cnt')))

    if checking_df.filter("checking = False").count() != 0:
        logger.error(f"[func sanity_check_for_counts] The customer counts of below regions/segments "
                     f"exceed {n_sigma} sigma of average percentage change in the last R12M.")
        
        err_records = checking_df.filter("checking = False")
        logger.error("\n"+getShowString(err_records, n=err_records.count(), truncate=False))
        raise ValueError()
    else:
        logger.info("[func sanity_check_for_counts] Sanity check passed, will continue updating ...")
    
    return


def load_data(dt, rename_to_new=True, join_partyid=True, regions=REGIONS):

    # need to happen the same date as the refresh
    _id = spark.sql('''
    select sk_partyid, sk_goldencustomerid
    from cip_analytics.vw_dimparty
    ''')
    current_refresh_dt = dtstring_to_date(dt)

    # load each region and combine
    df = load_segmentation_files(dt, regions)

    # adding current date
    df = df.withColumn('refresh_dt', f.lit(current_refresh_dt))

    if join_partyid:
        # join to get party_id level info
        df = df.join(_id, df.sk_goldencustomerid == _id.sk_goldencustomerid, how='inner').drop(_id.sk_goldencustomerid)
    else:
        df = df.withColumn('sk_partyid', f.lit(""))

    # rename to the correct order and adding new flag
    output = df.select(REFRESH_FILE_COLS)

    if rename_to_new:
        output = output.toDF(*['new_' + col for col in REFRESH_FILE_COLS])

    return output


def insert_new(df, output_columns):
    new = df.select(['new_' + col for col in output_columns]).toDF(*output_columns)
    return new


def perform_actions(df_merge):
    output_columns = STAGING1_COLS
    i = df_merge.filter(df_merge.action == 'INSERT')
    final = insert_new(i, output_columns)
    return final


def scd(stg1, stg0, dt):
    """
    stg1: is the permanent table with all party_id level history
    stg0: is the incremental update for each new refresh
    dt: is the date string for stg0
    """
    current_refresh_dt = dtstring_to_date(dt)

    # full outer join old and new data
    df_merge = stg1.join(stg0,
                         (stg1.sk_partyid == stg0.new_sk_partyid) & (stg1.region == stg0.new_region),
                         how='fullouter')

    # fill in NULLs in new side of data -> it becomes churn as new segment
    df_merge = df_merge.withColumn('new_region', f.coalesce('region', 'new_region')) \
        .withColumn('new_sk_partyid', f.coalesce('sk_partyid', 'new_sk_partyid')) \
        .withColumn('new_segment', f.coalesce('new_segment', f.lit('CHURN'))) \
        .withColumn('new_ret_score', f.coalesce('new_ret_score', f.lit(0.0))) \
        .withColumn('new_mb_score', f.coalesce('new_mb_score', f.lit(0.0))) \
        .withColumn('new_refresh_dt', f.coalesce('new_refresh_dt', f.lit(current_refresh_dt)))

    # make sure it's a newer update
    df_merge = df_merge.withColumn('is_newer_update',
                                   f.coalesce(df_merge.refresh_dt < df_merge.new_refresh_dt, f.lit(True)))
    df_merge = df_merge.filter(df_merge.is_newer_update == True).persist(StorageLevel.MEMORY_AND_DISK)

    if df_merge.count() > 0:
        # row level actions
        df_merge = df_merge.withColumn('action',
                                       f.when(df_merge.segment.isNull(), 'INSERT') \
                                       .when(df_merge.segment != df_merge.new_segment, 'INSERT') \
                                       .otherwise('KEEP')
                                       )

        incr_stg1 = perform_actions(df_merge)
        return incr_stg1
    else:
        logger.warning("[func scd] No new records are processed.")
        return None


def save_current_scores():
    df = load_data(dt, rename_to_new=False, join_partyid=False)
    save_table(df, current_score_table_name+'_tmp')
    spark.sql(f"drop table if exists {current_score_table_name}")
    spark.sql('''
    create table {current_score_table_name} as
    select *
    from {current_score_table_name}_tmp
    '''.format(current_score_table_name=current_score_table_name))
    return


def backup_table(table_name):
    if spark.catalog._jcatalog.tableExists(table_name):
        logger.info(f"[func backup_table] Backing up {table_name}")
        bk_table_name = table_name + '_bkp'
        spark.sql("drop table if exists {bk_table_name}".format(bk_table_name=bk_table_name))
        spark.sql("create table {bk_table_name} as select * from {table_name}".format(bk_table_name=bk_table_name,
                                                                                      table_name=table_name))
    else:
        logger.warning(f"[func backup_table] {table_name} does not exist! The backup has been skipped.")
    return


def prepare_for_histrecord(_df, region):
    df = _df.T.reset_index().rename({'index': 'segment'}, axis=1)
    df['region'] = region
    df['profile_dt'] = dt + " 00:00:00"
    if 'r12m_netsalesamt_total' not in df.columns:
        df['r12m_netsalesamt_total'] = df['custcnt'] * df['r12m_netsalesamt']
    df['custcnt'] = df['custcnt'].astype(int)

    final = df.loc[:, fields_to_keep]
    return final


def update_stg1():
    """
    This table keeps all history records used to re-compute stg2.
    """
    if table_exists(stg1_table_name):
        # refresh the migration
        _stg0 = load_data(dt)
        # select the latest record
        _stg1 = spark.sql('''
        with rnk as (
        select *, row_number() over (partition by region, sk_partyid order by refresh_dt desc) as rnk
        from {stg1_tbl_nm}
        )
        select {stg1_cols}
        from rnk
        where rnk = 1
        '''.format(stg1_tbl_nm=stg1_table_name,
                   stg1_cols = ', '.join(STAGING1_COLS)))
        stg1 = scd(_stg1, _stg0, dt)
    else:
        stg1 = load_data(dt, rename_to_new=False).select(STAGING1_COLS)

    if stg1:
        backup_table(stg1_table_name)
        save_table(stg1, stg1_table_name, mode='append')
    
    return


def compute_stg2():
    """
    KNOWN ISSUE
    Due to split/merge/re-assign of partyid to gcid, there are cases that different periods gcid would have different
    partyid mappings, thus it's possible that a prev_segment would be the same as the current segment.
    However, it doesn't invalidate the method - just duplicating small amout of records if any.
    Given a timeframe a gcid's segment is determined by the latest partyid (largest partyid belong to this gcid).
    """

    # use the latest sk_partyid's record
    backup_table(stg1_dedup_table_name)
    spark.sql("drop table if exists {stg1_dedup}".format(stg1_dedup=stg1_dedup_table_name))
    spark.sql('''
    create table {stg1_dedup} as
    select p.sk_goldencustomerid, s1.*
        , row_number() over (partition by p.sk_goldencustomerid, s1.region, s1.refresh_dt order by s1.sk_partyid desc) as valid_flg
    from {stg1} s1
    join cip_analytics.vw_dimparty p
        on s1.sk_partyid = p.sk_partyid
    '''.format(stg1_dedup=stg1_dedup_table_name, stg1=stg1_table_name))

    backup_table(stg2_table_name)
    spark.sql("drop table if exists {stg2}".format(stg2=stg2_table_name))
    spark.sql('''
    create table {stg2} as
    with migration as (
    select sk_goldencustomerid
        , region
        , segment
        , ret_score
        , mb_score
        , refresh_dt as start_dt
        , coalesce(cast(date_add(lead(refresh_dt) over (partition by sk_goldencustomerid, region order by refresh_dt), -1) as timestamp),
                   cast('9999-12-31 00:00:00' as timestamp)) as end_dt
        , coalesce(lag(segment) over (partition by sk_goldencustomerid, region order by refresh_dt), 'NEW') as prev_segment
        , case when lead(refresh_dt) over (partition by sk_goldencustomerid, region order by refresh_dt) IS NULL then 'Y' else 'N' end as current_flg
    from {stg1_dedup}
    where valid_flg = 1
    )
    select * from migration
    '''.format(stg1_dedup=stg1_dedup_table_name, stg2=stg2_table_name))
    return


def update_profile_history():
    histrecord_res = []
    for region in REGIONS:
        profile_csv_filename = os.path.join(PROFILE_REPORTS_PATH, region, "report_source_csv",
                                            f"{region}_segmentprofile_{dt}.csv")
        profile_raw = pd.read_csv(profile_csv_filename, index_col=0, keep_default_na=False)
        histrecord_res.append(prepare_for_histrecord(profile_raw, region))

    prof_hist = pd.concat(histrecord_res)
    prof_hist['profile_dt'] = pd.to_datetime(prof_hist['profile_dt'])  # convert string to timestamp
    if table_exists(profile_history_table_name):
        ph = spark.createDataFrame(prof_hist,
                                schema=read_table(profile_history_table_name).schema)
    else:
        ph = spark.createDataFrame(prof_hist)

    # backup existing table and append the results
    backup_table(profile_history_table_name)
    save_table(ph, profile_history_table_name, format="hive", mode="append")
    return


def getShowString(df, n=20, truncate=True, vertical=False):
    if isinstance(truncate, bool) and truncate:
        return df._jdf.showString(n, 20, vertical)
    else:
        return df._jdf.showString(n, int(truncate), vertical)


def checking():
    # check stg2
    stg2_check = spark.sql('''
        select start_dt
            , count(sk_goldencustomerid) as cnts
        from {stg2}
        group by start_dt
        order by start_dt desc
        '''.format(stg2=stg2_table_name))
    logger.info(f"[func checking] Checking {stg2_table_name}...")
    logger.info("\n"+getShowString(stg2_check))

    # check current_score
    curr_score_check = spark.sql(f"select region, count(1) as cnts from {current_score_table_name} group by region")
    logger.info(f"[func checking] Checking {current_score_table_name}...")
    logger.info("\n"+getShowString(curr_score_check))

    # check profile_history
    ph_check = spark.sql(f"select profile_dt, count(1) as cnts from {profile_history_table_name} group by profile_dt") \
        .orderBy(f.desc("profile_dt"))
    logger.info(f"[func checking] Checking {profile_history_table_name}...")
    logger.info("\n"+getShowString(ph_check))

    return


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":
    spark = SparkSession \
        .builder \
        .enableHiveSupport() \
        .appName("gcs_update_end_tables") \
        .getOrCreate()

    parser = argparse.ArgumentParser()
    parser.add_argument('--dt', type=str, action='store', dest='dt', default=datetime.date.today().__str__())
    parser.add_argument('--logfilename', type=str, action='store', dest='logfile_name', default="gcs_refresh.log")
    parser.add_argument('--skip_sanity_check', type=str2bool, action='store',dest='skip_sanity_check', default=False)
    args = parser.parse_args()
    dt = args.dt
    logfile_name = args.logfile_name
    skip_sanity_check = args.skip_sanity_check

    # setup logger
    logFormatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=logFormatter, level=logging.INFO)
    logger = logging.getLogger()
    # os.remove(logfile_name) if os.path.exists(logfile_name) else None

    # add file handler
    handler = logging.FileHandler(logfile_name)
    handler.setFormatter(logging.Formatter(logFormatter))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    logger.info("=" * 30)
    logger.info("RUNNING update_end_tables.py ...")
    
    if not skip_sanity_check:
        logger.info("[func main] Running sanity check for segmentation counts ...")
        sanity_check_for_counts(dt)

    logger.info(f"[func main] Updating staging_1 table {stg1_table_name} ...")
    update_stg1()

    logger.info(f"[func main] Computing the SCD migration table {stg2_table_name} ...")
    compute_stg2()

    logger.info(f"[func main] Updating the current scores table {current_score_table_name} ...")
    save_current_scores()

    logger.info(f"[func main] Updating the profile history table {profile_history_table_name} ...")
    update_profile_history()

    logger.info("[func main] Final check ...")
    checking()

    spark.stop()
