drop table if exists tb_analytics.dy_gcs_profile_1yr_transactions;
create table tb_analytics.dy_gcs_profile_1yr_transactions as
select tr.*
    , case when (salesaftermarkdownamt - discountsalesamt) / grosssalesamt >= 0.9 then (salesaftermarkdownamt - discountsalesamt)
           when (returnsaftermarkdownamt - discountreturnamt) / grossreturnamt >= 0.9 then - (returnsaftermarkdownamt - discountreturnamt)
           else 0 end as fullpricesalescal
    , case when (salesaftermarkdownamt - discountsalesamt) / grosssalesamt >= 0.9 then salesunits
           when (returnsaftermarkdownamt - discountreturnamt) / grossreturnamt >= 0.9 then -returnunits
           else 0 end as fullpriceunits
    , 1 - salesaftermarkdownamt / grosssalesamt as mkdnrate
    , discountsalesamt / salesaftermarkdownamt as dsctrate
    , case when salesaftermarkdownamt != grosssalesamt then 1 else 0 end as mkdntaken
    , case when discountsalesamt / salesaftermarkdownamt > 0.1 then 1 else 0 end as dscttaken
    , ip.prodcat
from tb_analytics.dy_gcs_facttransactions tr
JOIN tb_analytics.dy_gcs_dimitemprice ip
    ON tr.calendaryearnum = ip.calendaryearnum
    and tr.businessunit = ip.businessunit
    and tr.sk_itemid = ip.sk_itemid
where trans_dt BETWEEN add_months(${CURRENT_DATE}, -12) AND date_add(${CURRENT_DATE}, -1)
    and tr.channel in (${CHANNEL_STRING})
;


drop table if exists tb_analytics.dy_gcs_profile_saleskpisumm;
create table tb_analytics.dy_gcs_profile_saleskpisumm as
with custsales as (
select sk_goldencustomerid, regions
    , sum(salesaftermarkdownamt - discountsalesamt) as salesamt
    , sum(returnsaftermarkdownamt - discountreturnamt) as returnsamt
    , greatest(sum(salesaftermarkdownamt - discountsalesamt) - sum(returnsaftermarkdownamt - discountreturnamt), 0.0) as netsalesamt -- to offsets negative customers
    , greatest(sum(grosssalesamt - grossreturnamt), 0.0) as netgrossamt
    , sum(salesunits) as salesunits
    , sum(returnunits) as returnunits
    , greatest(sum(salesunits) - sum(returnunits), 0.0) as netunits -- to offset negative customers
    , count(distinct case when salereturnvoidflag = 'S' then ordernumber else NULL end) as salesorders
    , sum(fullpricesalescal) as netfullpricesales
    , sum(fullpriceunits) as netfullpriceunits
    , 1 - sum(salesaftermarkdownamt) / sum(grosssalesamt) as avgmkdnrate  -- sales only
    , sum(discountsalesamt) / sum(salesaftermarkdownamt) as avgdsctrate  -- sales only
    , max(case when salereturnvoidflag = 'S' and channel = 'RF' then 1 else 0 end) as purchased_RF
    , max(case when salereturnvoidflag = 'S' and channel = 'EC' then 1 else 0 end) as purchased_EC
    , max(case when salereturnvoidflag = 'S' and channel = 'RO' then 1 else 0 end) as purchased_RO
    , avg(case when salereturnvoidflag = 'S' and prodcat = 'sport'    then (salesaftermarkdownamt - discountsalesamt) / grosssalesamt else NULL end) sport_fp_pct
    , avg(case when salereturnvoidflag = 'S' and prodcat = 'jewelry'  then (salesaftermarkdownamt - discountsalesamt) / grosssalesamt else NULL end) jewelry_fp_pct
    , avg(case when salereturnvoidflag = 'S' and prodcat = 'handbags' then (salesaftermarkdownamt - discountsalesamt) / grosssalesamt else NULL end) handbags_fp_pct
    , avg(case when salereturnvoidflag = 'S' and prodcat = 'shoes'    then (salesaftermarkdownamt - discountsalesamt) / grosssalesamt else NULL end) shoes_fp_pct
    , avg(case when salereturnvoidflag = 'S' and prodcat = 'slg'      then (salesaftermarkdownamt - discountsalesamt) / grosssalesamt else NULL end) slg_fp_pct
    , avg(case when salereturnvoidflag = 'S' and prodcat = 'rtw'      then (salesaftermarkdownamt - discountsalesamt) / grosssalesamt else NULL end) rtw_fp_pct
from tb_analytics.dy_gcs_profile_1yr_transactions
group by sk_goldencustomerid, regions
),
region_visits as (
SELECT sk_goldencustomerid
    , regions
    , count(distinct channel) as chanvisited
    , count(distinct ip.prodcat) as catpurchased
FROM tb_analytics.dy_gcs_profile_1yr_transactions tr
JOIN tb_analytics.dy_gcs_dimitemprice ip
    ON tr.calendaryearnum = ip.calendaryearnum
    and tr.businessunit = ip.businessunit
    and tr.sk_itemid = ip.sk_itemid
GROUP BY sk_goldencustomerid, regions
),
md_promo_breakdown as (
select sk_goldencustomerid
    , regions
    , count(case when item_promo_breakdown = 'FP_ONLY' then sk_itemid else NULL end) as fp_only_itm_cnts
    , count(case when item_promo_breakdown = 'MD_ONLY' then sk_itemid else NULL end) as md_only_itm_cnts
    , count(case when item_promo_breakdown = 'PROMO_ONLY' then sk_itemid else NULL end) as promo_only_itm_cnts
    , count(case when item_promo_breakdown = 'MD+PROMO' then sk_itemid else NULL end) as md_n_promo_itm_cnts
from
(
    select sk_goldencustomerid
        , regions
        , sk_itemid
        , case when mkdntaken = 0 and dscttaken = 0 then 'FP_ONLY'
            when mkdntaken = 1 and dscttaken = 0 then 'MD_ONLY'
            when mkdntaken = 0 and dscttaken = 1 then 'PROMO_ONLY'
            when mkdntaken = 1 and dscttaken = 1 then 'MD+PROMO'
            else 'ERR' end as item_promo_breakdown
    from tb_analytics.dy_gcs_profile_1yr_transactions tr 
    where salereturnvoidflag = 'S'
) tmp
group by sk_goldencustomerid, regions
)
select gcid.sk_goldencustomerid, gcid.regions
    , netsalesamt 
    , netfullpricesales
    , case when netsalesamt / netgrossamt >= 0.0 and netsalesamt / netgrossamt <= 1.0 then netsalesamt / netgrossamt else -1.0 end as pctmsrp
    , case when avgmkdnrate >= 0.0 and avgmkdnrate <= 1.0 then avgmkdnrate else -1.0 end as avgsalesmkdnrate
    , case when avgdsctrate >= 0.0 and avgmkdnrate <= 1.0 then avgdsctrate else -1.0 end as avgsalesdsctrate
    , salesorders as visits
    , case when netfullpricesales / netsalesamt >= 0.0 and netfullpricesales / netsalesamt <= 1.0 then netfullpricesales / netsalesamt else -1.0 end as avgfullpricerate_dollars
    , case when netfullpriceunits / netunits >= 0.0 and netfullpriceunits / netunits <= 1.0 then netfullpriceunits / netunits else -1.0 end as avgfullpricerate_units
    , coalesce(netsalesamt / salesorders, -1.0) as aov
    , coalesce(netsalesamt / netunits, -1.0) as aur
    , coalesce(netunits / salesorders, -1.0) as upt
    , purchased_RF
    , purchased_EC
    , purchased_RO
    , coalesce(fp_only_itm_cnts, 0) as fp_only_itm_cnts
    , coalesce(md_only_itm_cnts, 0) as md_only_itm_cnts
    , coalesce(promo_only_itm_cnts, 0) as promo_only_itm_cnts
    , coalesce(md_n_promo_itm_cnts, 0) as md_n_promo_itm_cnts
    , case when rv.chanvisited > 1 then 1 else 0 end as r12m_isomnichannel
    , case when rv.catpurchased > 1 then 1 else 0 end as r12m_ismulticategorybuyer
    , sport_fp_pct
    , jewelry_fp_pct
    , handbags_fp_pct
    , shoes_fp_pct
    , slg_fp_pct
    , rtw_fp_pct
from tb_analytics.dy_gcs_feat_gcid gcid
join custsales
    on gcid.sk_goldencustomerid = custsales.sk_goldencustomerid
    and gcid.regions = custsales.regions
join region_visits rv
     on gcid.sk_goldencustomerid = rv.sk_goldencustomerid
    and gcid.regions = rv.regions
left join md_promo_breakdown m 
    on gcid.sk_goldencustomerid = m.sk_goldencustomerid
        and gcid.regions = m.regions
;


drop table if exists tb_analytics.dy_gcs_profile_demobhvrsumm;
create table tb_analytics.dy_gcs_profile_demobhvrsumm as
with combined_age as (
select sk_goldencustomerid
  , case when year(current_date()) - birthyear between 1 and 19 then 1
         when year(current_date()) - birthyear between 20 and 34 then 2
         when year(current_date()) - birthyear between 35 and 50 then 3
         when year(current_date()) - birthyear between 51 and 65 then 4
         when year(current_date()) - birthyear between 66 and 99 then 5
         else sk_agegroupid end as sk_agegroupid
from cip_analytics.vw_dimgoldencustomer
),
age as (
select gc.sk_goldencustomerid, a.agegroupdesc
from combined_age gc
join cip_analytics.vw_dimagegroup a
  on gc.sk_agegroupid = a.sk_agegroupid
),
demo as (
select gcid.sk_goldencustomerid
    , gcid.regions
    -- , gcid.isomnichannel
    -- , gcid.ismulticategorybuyer
    , case when birthyear >= 1928 and birthyear <= 1945 then 'silentgen'
           when birthyear >= 1946 and birthyear <= 1964 then 'babyboomers'
           when birthyear >= 1965 and birthyear <= 1980 then 'genx'
           when birthyear >= 1981 and birthyear <= 1996 then 'millennials'
           when birthyear >= 1997 and birthyear <= 2012 then 'genz'
           when g.agegenerationdesc = 'Generation Z' then 'genz'
           when g.agegenerationdesc = 'Millennials' then 'millennials'
           when g.agegenerationdesc = 'Generation X' then 'genx'
           when g.agegenerationdesc = 'Baby Boomers' then 'babyboomers'
           when g.agegenerationdesc = 'Silent Generation' then 'silentgen'
           else 'unknown' end as generation
    , age.agegroupdesc
    , coalesce(concat('"', hhi.hhincomerangedesc, '"'), 'Unknown') as hhincomerangedesc
    -- , gcid.favchan
    -- , gcid.mostcatspend
    , gcid.touristism as countryleveltourism
    , coalesce(gcd.mosaic_household, '') as mosaic_typ
    , coalesce(case when gcd.mosaic_household is null or gcd.mosaic_household = '' then 'Unknown'
                    when substr(gcd.mosaic_household, 1, 1) in ('A', 'B', 'D', 'F') then substr(gcd.mosaic_household, 1, 1)
                    when substr(gcd.mosaic_household, 1, 1) in ('C', 'E') then 'C/E'
                    when substr(gcd.mosaic_household, 1, 1) in ('O', 'G', 'K') then 'O/G/K'
                    when substr(gcd.mosaic_household, 1, 1) in ('U', '') then 'Unknown'
                    else 'Others' end, 'Unknown') as mosaic_grp
    , coalesce(case when gcd.ethnic_group = 'A' then 'Black'
        when gcd.ethnic_group = 'O' then 'HispanicLatino'
        when gcd.ethnic_group in ('E', 'G', 'I', 'J', 'K', 'L') then 'White'
        when gcd.ethnic_group in ('B', 'C', 'D', 'N') then 'Asian'
        when gcd.ethnic_group in ('F', 'H', 'M') then 'Others'
        when gcd.ethnic_group in ('Z', '') then 'Unknown' end, 'Unknown') as ethnic_grp
    , gc.emailablecorporate as emailable
    , gc.phoneable
    , gc.mailable
    , case when gc.sk_marketabilitytypeidcorporate in (-1, 4, NULL) then 'N' else 'Y' end as marketable
from tb_analytics.dy_gcs_feat_final gcid
join cip_analytics.vw_dimgoldencustomer gc
    on gc.sk_goldencustomerid = gcid.sk_goldencustomerid
left join cip_analytics.vw_dimhhincomerange hhi
    on gc.sk_hhincomerangeid = hhi.sk_hhincomerangeid
left join cip_analytics.vw_dimgoldencustomerdemographic gcd
    on gc.sk_goldencustomerid = gcd.sk_goldencustomerid
left join cip_analytics.vw_dimagegeneration as g
    on gc.sk_agegenerationid = g.sk_agegenerationid
left join age
    on gc.sk_goldencustomerid = age.sk_goldencustomerid
),
preferredmth as (
select sk_goldencustomerid, regions, mth
from (
    select row_number() over (partition by sk_goldencustomerid, regions order by netsales desc) as rnk
        , sk_goldencustomerid, regions, mth
    from (
        select sk_goldencustomerid
            , regions
            , month(trans_dt) as mth
            , sum(salesaftermarkdownamt - discountsalesamt) as netsales
        from tb_analytics.dy_gcs_profile_1yr_transactions tr
        group by sk_goldencustomerid, regions, month(trans_dt)
    ) tmp
) preferredmth
where rnk = 1
),
allhistnew as (
-- if a customer bought an item but itemid launchdate not found, the the newness would be 0.
-- Newness for a cateogry not purchased would be -1
select sk_goldencustomerid, regions, tr.prodcat
    , case when datediff(tr.trans_dt, ip.launchtimestamp) > 0 then datediff(tr.trans_dt, ip.launchtimestamp) else 0 end as newness
from tb_analytics.dy_gcs_profile_1yr_transactions tr
LEFT JOIN tb_analytics.dy_gcs_dimitemprice ip
    ON tr.calendaryearnum = ip.calendaryearnum
    and tr.businessunit = ip.businessunit
    and tr.sk_itemid = ip.sk_itemid
),
new as (
select sk_goldencustomerid, regions
    , avg(newness) as newness
    , avg(case when prodcat = 'sport'    then newness else NULL end) sport_newness
    , avg(case when prodcat = 'jewelry'  then newness else NULL end) jewelry_newness
    , avg(case when prodcat = 'handbags' then newness else NULL end) handbags_newness
    , avg(case when prodcat = 'shoes'    then newness else NULL end) shoes_newness
    , avg(case when prodcat = 'slg'      then newness else NULL end) slg_newness
    , avg(case when prodcat = 'rtw'      then newness else NULL end) rtw_newness
from allhistnew
group by sk_goldencustomerid, regions
),
country_bu as (
select distinct regions, l.country
from tb_analytics.dy_gcs_dimlocationinfo gdl
join cip_analytics.vw_dimlocation l
    on gdl.sk_locationid = l.sk_locationid
where l.locationtypecode in ('10', '25')
    and l.country <> "" 
    and regions in ('NA', 'EU', 'AS', 'JP')
),
homebu as (
select gc.sk_goldencustomerid
    , country_bu.regions as home_region
from cip_analytics.vw_dimgoldencustomer gc
left join tb_analytics.dy_gcs_dimcountrycode cc
    on gc.country = cc.alpha2
left join country_bu
    on gc.country = country_bu.country
),
final as (
select d.*
    , coalesce(homebu.home_region, 'U') as homeregion
    , case when homebu.home_region = d.regions then 'Domestic' else 'Foreign' end as regionleveltourism
    , coalesce(round(newness, 0), -1) as newness
    , coalesce(round(sport_newness, 0), -1) as sport_newness
    , coalesce(round(jewelry_newness, 0), -1) as jewelry_newness
    , coalesce(round(handbags_newness, 0), -1) as handbags_newness
    , coalesce(round(shoes_newness, 0), -1) as shoes_newness
    , coalesce(round(slg_newness, 0), -1) as slg_newness
    , coalesce(round(rtw_newness, 0), -1) as rtw_newness
    , coalesce(mth, -1) as preferredmth
from demo as d  
left join new on d.sk_goldencustomerid = new.sk_goldencustomerid and d.regions = new.regions
left join preferredmth on d.sk_goldencustomerid = preferredmth.sk_goldencustomerid and d.regions = preferredmth.regions
left join homebu on homebu.sk_goldencustomerid = d.sk_goldencustomerid
)
select * from final
;


drop table if exists tb_analytics.dy_gcs_profile_custtype;
create table tb_analytics.dy_gcs_profile_custtype as
with prev_pur as (
SELECT p.sk_goldencustomerid
    , l.regions
    , cal.daydate as trans_dt
    , lag(cal.daydate) over (PARTITION BY p.sk_goldencustomerid, l.regions order by cal.daydate) as prev_trans_dt
FROM cip_analytics.vw_factsalesdetail tr
JOIN cip_analytics.vw_dimparty p
    ON tr.sk_partyid = p.sk_partyid
JOIN cip_analytics.vw_dimgoldencustomer c
    ON p.sk_goldencustomerid = c.sk_goldencustomerid
JOIN cip_analytics.vw_dimday cal
    ON nvl(tr.sk_orderdateid, tr.sk_dayid) = cal.sk_dayid
JOIN tb_analytics.dy_gcs_dimlocationinfo l  -- exclude invalid locations
    ON tr.sk_originatinglocationid = l.sk_locationid
WHERE sk_currencyversionid = 3
    AND salereturnvoidflag = 'S'
    AND c.rfeflag = 'N'
    AND l.channel in (${CHANNEL_STRING})
GROUP BY p.sk_goldencustomerid, l.regions, cal.daydate
),
custtyptrans as (
select *
    , case when prev_trans_dt is null then '1-New'
           when datediff(trans_dt, prev_trans_dt)  > 365 then '2-Reactivated'
           when datediff(trans_dt, prev_trans_dt)  <= 365 then '3-Retained'
           else '4-Error' end as custtyp
from prev_pur
),
final as (
select sk_goldencustomerid, regions, min(custtyp) as custtype
from custtyptrans
where trans_dt between add_months(${CURRENT_DATE}, -12) AND date_add(${CURRENT_DATE}, -1)
group by sk_goldencustomerid, regions
)
select *
from final
;


drop table if exists tb_analytics.dy_gcs_gcid_ltv;
create table tb_analytics.dy_gcs_gcid_ltv as
SELECT c.sk_goldencustomerid, l.regions
    , sum(grosssalesamt-(salescalcmarkdownamt-returncalcmarkdownamt) - (grossreturnamt-discountreturnamt)-discountsalesamt) ltv
FROM cip_analytics.vw_factsalesdetail tr
JOIN cip_analytics.vw_dimparty p
    ON tr.sk_partyid = p.sk_partyid
JOIN tb_analytics.dy_gcs_feat_final c
    ON p.sk_goldencustomerid = c.sk_goldencustomerid
JOIN cip_analytics.vw_dimday cal
    ON nvl(tr.sk_orderdateid, tr.sk_dayid) = cal.sk_dayid
JOIN tb_analytics.dy_gcs_dimlocationinfo l  -- exclude invalid locations
    ON tr.sk_originatinglocationid = l.sk_locationid
WHERE sk_currencyversionid = 3
    AND salereturnvoidflag <> 'V'
    AND cal.daydate < ${CURRENT_DATE}
    AND l.channel in (${CHANNEL_STRING})
group by c.sk_goldencustomerid, l.regions
;


drop table if exists tb_analytics.dy_gcs_retention_mb_nextyear;
create table tb_analytics.dy_gcs_retention_mb_nextyear as
with retainedgcid as (
select sk_goldencustomerid, regions
    , case when validvisits = 0 then 1 else validvisits end as validvisits
from (
    select gc.sk_goldencustomerid, gc.regions
        , count(distinct case when t.isreturned = 'N' then t.trans_dt else NULL end) as validvisits
    from tb_analytics.dy_gcs_feat_gcid gc
    join tb_analytics.dy_gcs_factsalestransactions t
    where gc.sk_goldencustomerid = t.sk_goldencustomerid
        and gc.regions = t.regions
        and t.trans_dt > gc.lastdt
        and t.trans_dt < date_add(gc.lastdt, 365)
        and t.channel in (${CHANNEL_STRING})
    GROUP BY gc.sk_goldencustomerid, gc.regions
    ) x
)
, mbgcid as (
select sk_goldencustomerid, regions
    , case when validvisits = 0 then 1 else validvisits end as validvisits
    , case when validvisits > 1 then 1 else 0 end as ismultibuyer
from (
    select gc.sk_goldencustomerid, gc.regions
        , count(distinct case when t.isreturned = 'N' then t.trans_dt else NULL end) as validvisits
    from tb_analytics.dy_gcs_feat_gcid gc
    join tb_analytics.dy_gcs_factsalestransactions t
    where gc.sk_goldencustomerid = t.sk_goldencustomerid
        and gc.regions = t.regions
        and t.trans_dt BETWEEN ${CURRENT_DATE} AND date_add(add_months(${CURRENT_DATE}, 12), -1)
        and t.channel in (${CHANNEL_STRING})
    GROUP BY gc.sk_goldencustomerid, gc.regions
) x    
)
select id.sk_goldencustomerid
    , id.regions
    , case when r.sk_goldencustomerid is null then 0 else 1 end as isretained
    , case when rmb.ismultibuyer is null then 0 else rmb.ismultibuyer end as ismultibuyernextyear
FROM tb_analytics.dy_gcs_feat_gcid id
left join retainedgcid r
    ON id.sk_goldencustomerid = r.sk_goldencustomerid
    AND id.regions = r.regions
LEFT JOIN mbgcid rmb
    ON id.sk_goldencustomerid = rmb.sk_goldencustomerid
    AND id.regions = rmb.regions
;


drop table if exists tb_analytics.dy_gcs_profile_final;
create table tb_analytics.dy_gcs_profile_final as
select d.sk_goldencustomerid
    , d.regions
    , c.custtype
    , d.mosaic_grp
    , d.mosaic_typ
    , d.ethnic_grp
    , d.marketable
    , d.emailable
    , d.phoneable
    , d.mailable
    -- , d.isomnichannel
    -- , d.ismulticategorybuyer
    , d.generation
    , d.agegroupdesc
    , d.hhincomerangedesc
    -- , d.favchan
    -- , d.mostcatspend
    , d.homeregion
    , d.countryleveltourism
    , d.regionleveltourism
    , d.newness
    , d.sport_newness
    , d.jewelry_newness
    , d.handbags_newness
    , d.shoes_newness
    , d.slg_newness
    , d.rtw_newness
    , d.preferredmth
    , s.netsalesamt
    , s.avgfullpricerate_dollars
    , s.avgfullpricerate_units
    , s.pctmsrp
    , s.avgsalesmkdnrate
    , s.avgsalesdsctrate
    , s.visits
    , s.purchased_RF
    , s.purchased_EC
    , s.purchased_RO
    , s.aov
    , s.aur
    , s.upt
    , s.fp_only_itm_cnts
    , s.md_only_itm_cnts
    , s.promo_only_itm_cnts
    , s.md_n_promo_itm_cnts
    , s.r12m_isomnichannel
    , s.r12m_ismulticategorybuyer
    , s.sport_fp_pct
    , s.jewelry_fp_pct
    , s.handbags_fp_pct
    , s.shoes_fp_pct
    , s.slg_fp_pct
    , s.rtw_fp_pct
    , ltv.ltv
    , ny.isretained
    , ny.ismultibuyernextyear
from tb_analytics.dy_gcs_profile_demobhvrsumm d  
join tb_analytics.dy_gcs_profile_custtype c 
    on d.sk_goldencustomerid = c.sk_goldencustomerid
    and d.regions = c.regions
join tb_analytics.dy_gcs_profile_saleskpisumm s  
    on s.sk_goldencustomerid = d.sk_goldencustomerid
    and s.regions = d.regions
join tb_analytics.dy_gcs_gcid_ltv ltv
    on ltv.sk_goldencustomerid = d.sk_goldencustomerid
    and ltv.regions = d.regions
join tb_analytics.dy_gcs_retention_mb_nextyear ny
    on ny.sk_goldencustomerid = d.sk_goldencustomerid
    and ny.regions = d.regions
;